import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:convert';
import 'dart:math';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/laguages/Languages.dart';
import 'package:iziway/laguages/locale_constant.dart';
import 'package:path/path.dart' as path;
import 'package:crypto/crypto.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../app_theme.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class Utils {
  BuildContext context;

  Utils({this.context});

  /*Future<bool> isNetworkAvailable(bool showDialog) async {
    ConnectivityResult _result;
    final Connectivity _connectivity = Connectivity();
    try {
      _result = await _connectivity.checkConnectivity();
      print(_result);
      switch (_result) {
        case ConnectivityResult.wifi:
          return true;
        case ConnectivityResult.mobile:
          return true;
        default:
          if (showDialog) {
            alertDialog(Languages.of(context).msgNotAvailableInternet);
          }
          return false;
      }
    } on PlatformException catch (e) {
      print(e.toString());
      if (showDialog) {
        alertDialog(Languages.of(context).msgNotAvailableInternet);
      }
      return false;
    }
  }*/

  /// <<< To check Network is Available or not --------- >>>
  Future<bool> isNetworkAvailable(
    BuildContext context,
    Utils _utils, {
    bool showDialog,
  }) async {
    ConnectivityResult _result;
    final Connectivity _connectivity = Connectivity();
    try {
      _result = await _connectivity.checkConnectivity();
      // loggerPrint(_result);
      switch (_result) {
        case ConnectivityResult.wifi:
          return true;
        case ConnectivityResult.mobile:
          return true;
        default:
          if (showDialog) {
            // _utils?.hideProgressDialog(context!);
            alertDialog(Languages.of(context).msgNotAvailableInternet);
          }
          return false;
      }
    } on PlatformException catch (e) {
      // loggerPrint(e.toString());
      if (showDialog) {
        // _utils?.hideProgressDialog(context!);
        // dialogAlert(context!, Strings.interNetNotAvailable);
        alertDialog(e.message);
      }
      return false;
    }
  }

  Widget flatButton(String btnName, VoidCallback onPressed) {
    return TextButton(
      style: TextButton.styleFrom(
        foregroundColor: AppTheme.Green,
        padding: const EdgeInsets.all(16.0),
        textStyle: const TextStyle(fontSize: 20),
      ),
      onPressed: onPressed,
      child: Text(btnName),
    );
  }

  void alertDialog(String mMsg) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                // Languages.of(context).appName,
                Constant.appName,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              content: Text(mMsg, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
              actions: <Widget>[
                /*FlatButton(
          child: Text(Languages.of(context).btnOk),
          onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
          },
          ),*/
                flatButton(Languages.of(context).btnOk, () {
                  Navigator.of(context, rootNavigator: true).pop();
                }),
              ]);
        });
  }

  void alertDialogWithClickHandle(String mMsg, VoidCallback onPressedClick) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                // Languages.of(context).appName,
                Constant.appName,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              content: Text(mMsg, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
              actions: <Widget>[
                /*FlatButton(
                  child: Text(Languages.of(context).btnOk),
                  */ /*onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    onPressed;
                  }*/ /*
                  onPressed: onPressedClick,
                ),*/

                flatButton(Languages.of(context).btnOk, onPressedClick),
              ]);
        });
  }

  void alertDialogWithYesNoClickHandle(String mMsg, VoidCallback onPressedClick) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                // Languages.of(context).appName,
                Constant.appName,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              content: Text(mMsg, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
              actions: <Widget>[
                /* FlatButton(
                  child: Text(Languages.of(context).btnNo),
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),*/
                flatButton(Languages.of(context).btnNo, () {
                  Navigator.of(context, rootNavigator: true).pop();
                }),
                /*FlatButton(
                  child: Text(Languages.of(context).btnYes),
                  */ /*onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    onPressed;
                  }*/ /*
                  onPressed: onPressedClick,
                ),*/
                flatButton(Languages.of(context).btnYes, onPressedClick),
              ]);
        });
  }

  bool isValidationEmpty(String val) {
    if (val == null || val.isEmpty || val == Constant.validationNullLowerCase || val == "" || val.length == 0 || val == Constant.validationNullUpperCase) {
      return true;
    } else {
      return false;
    }
  }

  bool emailValidator(String email) {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(email)) {
      return true;
    }

    return false;
  }

  void showProgressDialog(String isFrom) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: Container(
                child: Center(
              child: Container(
                  width: 110.0,
                  height: 100.0,
                  decoration: BoxDecoration(color: isFrom == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow, borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      SizedBox(height: 20),
                      SpinKitPouringHourglass(color: Colors.white, size: 40.0),
                      SizedBox(height: 20),
                      Text(
                        Languages.of(context).msgLoading,
                        style: TextStyle(decoration: TextDecoration.none, color: Colors.white, fontSize: 12, fontWeight: FontWeight.w500),
                      )
                    ],
                  )),
            )),
          );
        });
  }

  void hideProgressDialog() {
    Navigator.of(context, rootNavigator: true).pop();
  }

  String getDeviceType() {
    if (Platform.isAndroid) {
      return Constant.deviceTypeAndroid;
    } else {
      return Constant.deviceTypeIos;
    }
  }

  daysInMonth(int monthNum, int year) {
    List<int> monthLength = new List(12);

    monthLength[0] = 31;
    monthLength[2] = 31;
    monthLength[4] = 31;
    monthLength[6] = 31;
    monthLength[7] = 31;
    monthLength[9] = 31;
    monthLength[11] = 31;
    monthLength[3] = 30;
    monthLength[8] = 30;
    monthLength[5] = 30;
    monthLength[10] = 30;

    if (leapYear(year) == true)
      monthLength[1] = 29;
    else
      monthLength[1] = 28;

    return monthLength[monthNum - 1];
  }

  bool leapYear(int year) {
    bool leapYear = false;
    bool leap = ((year % 100 == 0) && (year % 400 != 0));
    if (leap == true)
      leapYear = false;
    else if (year % 4 == 0) leapYear = true;
    return leapYear;
  }

  Future<String> getFileNameWithExtension(File file) async {
    if (await file.exists()) {
      //To get file name without extension
      //path.basenameWithoutExtension(file.path);
      //return file with file extension
      return path.basename(file.path);
    } else {
      return "";
    }
  }

  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> getLocation(Utils _utils) async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      // return Future.error(Languages.of(context).msgLocationDisable);
      // _utils.alertDialog(Languages.of(context).msgLocationDisable);
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        // return Future.error('Location permissions are denied');
        // _utils.alertDialog('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      // return Future.error(
      //     Languages.of(context).msgDeniedForever);
      _utils.alertDialog(Languages.of(context).msgDeniedForever);
      return Future.error(Languages.of(context).msgDeniedForever);
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  void alertDialogImagePreview(String image) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(
            alignment: Alignment.center,
            child: Stack(
              children: [
                /*FadeInImage.assetNetwork(
              placeholder: 'assets/images/app_logo.png',
              image: image,
            ),*/
                PinchZoom(
                  child: Image.network(image,
                      /* errorBuilder: (context, error, stackTrace) {
                print('image_network_error: ' + error);
                return Center(
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/images/app_logo.png',
                    image: image,
                  ),
                );
              },*/
                      loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
                    if (loadingProgress != null && loadingProgress.expectedTotalBytes != null) {
                      return Center(
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes : 0,
                        ),
                      );
                    } else {
                      return child;
                    }
                  }),

                  // zoomedBackgroundColor: AppTheme.black.withOpacity(0.5),
                  /*zoomedBackgroundColor: Constant.profileType == Constant.profileTypeUser
                  ? AppTheme.Green.withOpacity(0.5)
                  : AppTheme.Yellow.withOpacity(0.5),*/
                  resetDuration: const Duration(milliseconds: 100),
                  maxScale: 2.5,
                  // onZoomStart: (){print('Start zooming');},
                  // onZoomEnd: (){print('Stop zooming');},
                ),
                Container(
                  padding: EdgeInsets.all(ResponsiveFlutter.of(context).scale(16)),
                  alignment: AlignmentDirectional.topEnd,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    child: Icon(
                      Icons.cancel,
                      size: ResponsiveFlutter.of(context).scale(32),
                      color: Constant.profileType == Constant.profileTypeAgent ? AppTheme.Yellow : AppTheme.Green,
                    ),
                  ),
                )
              ],
            ),

            width: double.infinity,
            height: double.infinity,
            // margin: EdgeInsets.all(ResponsiveFlutter.of(context).scale(16)),
          );
        });
  }

  void dialogChooseLanguage(Locale _locale) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Material(
            type: MaterialType.transparency,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(12)),
                  margin: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(16)),
                  decoration: BoxDecoration(color: AppTheme.white, borderRadius: BorderRadius.circular(10)),
                  alignment: AlignmentDirectional.center,
                  child: Column(
                    children: [
                      Text(
                        Languages.of(context).btnChangeLanguage,
                        style: GoogleFonts.roboto(fontSize: ResponsiveFlutter.of(context).fontSize(2.5), fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                      ),
                      SizedBox(height: ResponsiveFlutter.of(context).verticalScale(16)),
                      GestureDetector(
                        onTap: () {
                          changeLanguage(context, Constant.languageCodeEn);
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        child: Container(
                          alignment: AlignmentDirectional.center,
                          child: Text(
                            Constant.languageNameEnglish,
                            style: GoogleFonts.roboto(
                                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                fontWeight: FontWeight.w500,
                                color: _locale.languageCode == Constant.languageCodeEn
                                    ? Constant.profileType == Constant.profileTypeAgent
                                        ? AppTheme.Yellow
                                        : AppTheme.Green
                                    : AppTheme.Grey),
                          ),
                        ),
                      ),
                      SizedBox(height: ResponsiveFlutter.of(context).verticalScale(12)),
                      Divider(
                        color: AppTheme.Grey,
                        height: ResponsiveFlutter.of(context).verticalScale(1),
                      ),
                      SizedBox(height: ResponsiveFlutter.of(context).verticalScale(12)),
                      GestureDetector(
                        onTap: () {
                          changeLanguage(context, Constant.languageCodeFr);
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        child: Container(
                          alignment: AlignmentDirectional.center,
                          child: Text(
                            Constant.languageNameFrench,
                            style: GoogleFonts.roboto(
                                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                fontWeight: FontWeight.w500,
                                color: _locale.languageCode == Constant.languageCodeFr
                                    ? Constant.profileType == Constant.profileTypeAgent
                                        ? AppTheme.Yellow
                                        : AppTheme.Green
                                    : AppTheme.Grey),
                          ),
                        ),
                      ),
                      SizedBox(height: ResponsiveFlutter.of(context).verticalScale(12)),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  String getSubscriptionPlan(String planDays, String price) {
    if (planDays == Constant.subscriptionPlan1Day && price == Constant.subscriptionPlan1Price) {
      return Languages.of(context).subscriptionPlan1;
    } else if (planDays == Constant.subscriptionPlan2Day && price == Constant.subscriptionPlan2Price) {
      return Languages.of(context).subscriptionPlan2;
    } else if (planDays == Constant.subscriptionPlan3Day && price == Constant.subscriptionPlan3Price) {
      return Languages.of(context).subscriptionPlan3;
    } else if (planDays == Constant.subscriptionPlan4Day && price == Constant.subscriptionPlan4Price) {
      return Languages.of(context).subscriptionPlan4;
    } else {
      /*  return "Forfait actuel (" +
          planDays +
          " Jours): " +
          price +
          " FCFA"; */ // info: default plan // : pending localization
      return Languages.of(context).getDefaultPlan(planDays, price);
    }
  }

  double getDistanceFromGPSPointsInRoute(List<LatLng> data) {
    /* double totalDistance = 0.0;

    for (var i = 0; i < gpsList.length; i++) {
      var p = 0.017453292519943295;
      var c = cos;
      var a = 0.5 -
          c((gpsList[i + 1].latitude - gpsList[i].latitude) * p) / 2 +
          c(gpsList[i].latitude * p) *
              c(gpsList[i + 1].latitude * p) *
              (1 - c((gpsList[i + 1].longitude - gpsList[i].longitude) * p)) /
              2;
      double distance = 12742 * asin(sqrt(a));
      totalDistance += distance;
      print('Distance is ${12742 * asin(sqrt(a))}');
    }
    print('Total distance is $totalDistance');
    return totalDistance;*/

    double calculateDistance(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;
      var c = cos;
      var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
      return 12742 * asin(sqrt(a));
    }

    /*List<dynamic> data = [
       {
         "lat": 44.968046,
         "lng": -94.420307
       },{
         "lat": 44.33328,
         "lng": -89.132008
       },{
         "lat": 33.755787,
         "lng": -116.359998
       },{
         "lat": 33.844843,
         "lng": -116.54911
       },{
         "lat": 44.92057,
         "lng": -93.44786
       },{
         "lat": 44.240309,
         "lng": -91.493619
       },{
         "lat": 44.968041,
         "lng": -94.419696
       },{
         "lat": 44.333304,
         "lng": -89.132027
       },{
         "lat": 33.755783,
         "lng": -116.360066
       },{
         "lat": 33.844847,
         "lng": -116.549069
       },
     ];*/

    double totalDistance = 0;
    for (var i = 0; i < data.length - 1; i++) {
      totalDistance += calculateDistance(data[i].latitude, data[i].longitude, data[i + 1].latitude, data[i + 1].longitude);
    }
    print(totalDistance);
    return totalDistance;
  }

/*double removePointValue(Utils _utils, double totalDistance) {
    // return (totalDistance).round();
    if (!_utils.isValidationEmpty(totalDistance.toString()) &&
        totalDistance > 0) {
      // return (totalDistance).round();
      return double.parse(totalDistance.toStringAsFixed(2));
    } else {
      return 0;
    }
  }*/

/* String getDurationTime(double totalDistance) {
    final diff_hr = currentTime
        .difference(startTime)
        .inHours;
    final diff_mn = currentTime
        .difference(startTime)
        .inMinutes;
  }*/

/* getDurationAndDistance(String googleApiKey, double originLat, double originLong,
      double destLat, double destLong) async {
    */ /*Dio dio = new Dio();
    Response response = await dio.get(
        "https://maps.googleapis.com/maps/api/directions/json?origin=21.2305574,72.901545&destination=21.2316919,72.8662349&sensor=false&units=metric&mode=driving&key=AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY");*/ /*
    print("test_start");
    // print(response.data);
    // print("test_end");

    */ /*  String distance =
        json.decode(response.data)["routes"][0]["legs"]["distance"]["text"];
    print("test_distance: "+distance);

    String duration =
        json.decode(response.data)["routes"][0]["legs"]["duration"]["text"];
    print("test_duration: "+duration);*/ /*

    // String url = "https://maps.googleapis.com/maps/api/directions/json?origin=21.2305574,72.901545&destination=21.2316919,72.8662349&sensor=false&units=metric&mode=driving&key=AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY";

    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" +
        originLat.toString() +
        "," +
        originLong.toString() +
        "&destination=" +
        destLat.toString() +
        "," +
        destLong.toString() +
        "&mode=driving" +
        "&key=$googleApiKey";

    var response = await http.get(url);
    // print('test_body: ' + response.body);

    try {
      if (response?.statusCode == 200) {
        */ /*Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> data = map["routes"];
        print(data[0]["legs"]);
        List<dynamic> data1 = data[0]["legs"];
        print(data1[0]["distance"]);
        print("test_distance_text: "+data1[0]["distance"]["text"]);

        print(data1[0]["duration"]);
        print("test_duration_text: "+data1[0]["duration"]["text"]);

        print('test_end');*/ /*

        Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> routes = map["routes"];
        // print(data[0]["legs"]);
        List<dynamic> legs = routes[0]["legs"];
        // print(data1[0]["distance"]);
        dynamic distance=legs[0]["distance"];
        print("test_distance_text: "+distance["text"]);

        // print(data1[0]["duration"]);
        dynamic duration=legs[0]["duration"];
        print("test_duration_text: "+duration["text"]);

        print('test_end');
      }
    } catch (error) {
      print("test_error");
      throw Exception(error.toString());
    }
  }*/

  String customDateTimeFormat(Utils _utils, String dateTime /*, String inputDateFormat*/, String outputDateFormat) {
    if (!_utils.isValidationEmpty(dateTime)) {
      // final DateTime now = DateTime.now();
      final DateTime now = DateTime.parse(dateTime);
      final DateFormat formatter = DateFormat(outputDateFormat);
      return formatter.format(now);
    } else {
      return "";
    }
  }
}
