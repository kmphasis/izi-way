import 'package:geolocator/geolocator.dart';

import 'Languages.dart';

class LanguageEn extends Languages {
  /* @override
  String get appName => 'IZI Way';*/

  @override
  String get selectProfile => 'Please choose a profile picture';

  @override
  String get enterName => 'Please enter your name';

  @override
  String get enterPhone => 'Please enter your phone number';

  @override
  String get enterEmail => 'Please enter your email';

  @override
  String get enterValidEmail => 'Please enter a correct email address';

  @override
  String get enterPassword => 'Please enter your password';

  @override
  String get enterConPassword => 'Please confirm your password';

  @override
  String get bothPasswordNotMatch => 'The two passwords do not match';

  @override
  String get enterAgentId => 'Please enter the agent ID';

  @override
  String get selectImage => 'Please select at least one image';

  @override
  String get enterDescription => 'Please enter description';

  @override
  String get enableLocationPermission => 'Please allow localization';

  @override
  String get msgError => 'Something went wrong, please try again ...';

  @override
  String get msgNotAvailableInternet =>
      'Your internet is not available, please try again later.';

  @override
  String get textForte => 'Strong';

  @override
  String get textMoyen => 'Way';

  @override
  String get textFaible => 'Low';

  @override
  String get textEnvoyer => 'To send';

  @override
  String get btnPurchase => 'SUBSCRIBE';

  @override
  String get btnFreeTry => 'Try (7 days)';

  @override
  String get btnTryNow => 'Try now';

  @override
  String get btnSubscription => 'SUBSCRIPTIONS';

  @override
  String get btnLogout => 'Logout';

  @override
  String get btnOk => 'okay';

  @override
  String get btnNo => 'No';

  @override
  String get btnYes => 'Yes';

  @override
  String get btnRefUser => 'Refuse';

  @override
  String get btnAccept => 'Accept';

  @override
  String get btnParametres => 'settings';

  @override
  String get msgLoading => 'Loading...';

  @override
  String get msgDeniedForever =>
      'Location permissions are permanently denied, we cannot request permissions.';

  @override
  String get validEnterComment => 'Please enter your comments here';

  @override
  String get msgLocationPermissionDisable =>
      'Location services are turned off.';

  @override
  String get titleEditProfile => 'modify the profile';

  @override
  String get lblLastName => 'Name';

  @override
  String get validEnterName => 'Enter your name';

  @override
  String get lblEmail => 'E-mail';

  @override
  String get validEnterEmail => 'Enter your e-mail';

  @override
  String get lblPhoneNo => 'Phone number';

  @override
  String get validEnterPhoneNo => 'Enter your phone number';

  @override
  String get btnRecord => 'Record';

  @override
  String get lblForgotPassword => 'Forgot your password?';

  @override
  String get decForgotPassword =>
      'Please enter the email address associated with your email. We will send you a link to reset your password.';

  @override
  String get lblPassword => 'Password';

  @override
  String get validEnterPassword => 'Enter your password';

  @override
  String get btnSubmit => 'To send';

  @override
  String get titleAccessLocalization => 'Access the location';

  @override
  String get hintSearchLocation => 'Search Location...';

  @override
  String get introduction1 =>
      'Drive with IZI WAY without fear of traffic jams!';

  @override
  String get introduction2 => 'Ride well surrounded by scouts with IZI WAY';

  @override
  String get introduction3 =>
      'Drive knowing the traffic conditions in real time';

  @override
  String get introduction4 => 'Avoid the road signs with the fastest path';

  @override
  String get confirmMsgLogout => 'Are you sure you want to log out?';

  @override
  String get titleLicenseToUse => 'License to use';

  @override
  String get msgLicenseToUse =>
      // 'Lorem Ipsum est simplement un texte factice de l\'industrie de l\'impression et de la composition. Lorem Ipsum est le texte factice standard de l\'industrie depuis les années 1500, lorsqu\'un imprimeur inconnu a pris une galère de caractères et l\'a brouillé pour en faire un spécimen de type livre. C\'était dans les années 1960 avec la sortie des feuilles Letraset contenant des passages de Lorem Ipsum, et plus récemment avec des logiciels de PAO comme Aldus PageMaker incluant des versions de Lorem Ipsum.';

      /*'<p>Merci d\’utiliser IZI WAY !<br><br>'
          'Cette politique de confidentialité décrit :<br><br>'
          'Les façons dont nous collectons les données de trafic routier et pourquoi nous le faisons. Comment nous traitons ces données et les renvoyons à la communauté IZI WAY.<br><br>'
          'Les choix dont vous disposez concernant ces données trafic. La présente Politique de confidentialité s\'applique à IZI WAY, que nous appelons icicollectivement le Service. Nous pouvons périodiquement mettre à jour cette Politique deconfidentialité en publiant une nouvelle version sur <a href="https://Africanity-group.com">https://Africanity-group.com</a>.<br><br>'
          'Si nous apportons des modifications importantes, nous vous en informerons en publiant un avis dans le Service avant que la modification ne prenne effet. Votre utilisation continue du Service après la date d\'entrée en vigueur sera soumise à la nouvelle Politique deconfidentialité.<br><br>'
          'Ⓒ Juillet 2021</p>';*/

      'Thank you for using IZI WAY!\n\n'
      'This privacy policy describes:\n\n'
      'The ways we collect road traffic data and why we do it. How we process this data and send it back to the IZI WAY community.\n\n'
      'The choices you have regarding this traffic data. This Privacy Policy applies to IZI WAY, which we collectively refer to as the Service. We may periodically update this Privacy Policy by posting a new version on https://Africanity-group.com.\n\n'
      'If we make any material changes, we will let you know by posting a notice in the Service before the change takes effect. Your continued use of the Service after the effective date will be subject to the new Privacy Policy.\n\n'
      'Ⓒ July 2021';

  @override
  String get msgLicenseToUse_1 => 'Thank you for using IZI WAY!\n\n';

  @override
  String get msgLicenseToUse_2 =>
      'This privacy policy describes:\n\n';

  @override
  String get msgLicenseToUse_3 =>
      'The ways we collect road traffic data and why we do it. How we process this data and send it back to the IZI WAY community.\n\n';

  @override
  String get msgLicenseToUse_4 =>
      'The choices you have regarding this traffic data. This Privacy Policy applies to IZI WAY, which we collectively refer to as the Service. We may periodically update this Privacy Policy by posting a new version on ';

  @override
  String get msgLicenseToUse_5 => 'https://Africanity-group.com';

  @override
  String get msgLicenseToUse_6 => '.\n\n';

  @override
  String get msgLicenseToUse_7 =>
      'If we make any material changes, we will let you know by posting a notice in the Service before the change takes effect. Your continued use of the Service after the effective date will be subject to the new Privacy Policy.\n\n';

  @override
  String get msgLicenseToUse_8 => 'Ⓒ July 2021';

  @override
  String getMsgUrlLaunch(String url) {
    return 'Could not launch $url';
  }

  @override
  String get msgLocationPermission1 =>
      'IZI WAY needs the location to be able to function normally';

  @override
  String get hintRememberMe => 'Remember me?';

  @override
  String get btnResendConfirmLink => 'Resend confirmation link';

  @override
  String get btnToLogin => 'TO LOG IN';

  @override
  String get btnSubscribe => 'SUBSCRIBE';

  @override
  String get btnMessaging => 'Messaging';

  @override
  String get hintBy => 'Through';

  @override
  String get hintEnterPassword => 'Enter the password';

  @override
  String get hintEnterConfirmPassword => 'Enter the password again';

  @override
  String get lblConfirmPassword => 'Confirm password';

  @override
  String get lblAgentId => 'Agent ID';

  @override
  String get hintEnterId => 'Please enter ID here';

  @override
  String get lblRegisterAs => 'Register as';

  @override
  String get lblUser => 'User';

  @override
  String get lblAgentEclaireur => 'Scout Agent';

  @override
  String get titleMakeAnAlert => 'Make an alert';

  @override
  String get lblByAfricanityGroup => 'By AFRICANITY GROUP';

  @override
  String get subscriptionPlan1 => 'Monthly (30 Days): 1000 FCFA';

  @override
  String get subscriptionPlan2 => 'Quarterly (90 Days): 2500 FCFA';

  @override
  String get subscriptionPlan3 => 'Semester (180 Days): 5,000 FCFA';

  @override
  String get subscriptionPlan4 => 'Annual (365 Days): 10,000 FCFA';

  @override
  String get titleSubscriptions => 'Subscriptions';

  @override
  String get msgPurchaseFailed => 'The purchase failed!';

  @override
  String get msgPurchaseSuccess => 'Successful purchase';

  @override
  String get lblGoPremium => 'Upgrade to premium version';

  @override
  String get lblGetUnlimitedAccess => 'Get unlimited access';

  @override
  String get contentSubscription =>
      'Lorem Ipsum is just dummy text from the print.'; // TODO: pending: dummy content

  @override
  String get validSubscriptionPlan =>
      'Choose your subscription and try again ...';

  @override
  String get titleEquipeIZIWay => 'IZI WAY team';

  @override
  String get titleMyIziWay => 'My IZI WAY';

  @override
  String get lblUserAccount => 'User account';

  @override
  String get btnSettings => 'Settings';

  @override
  String get titlePayment => 'Payment';

  @override
  String get btnChangeLanguage => 'Change language';

  @override
  String getLocationPermissionDeniedWithName(LocationPermission permission) {
    return 'Location permissions are denied (actual value: $permission).';
  }

  @override
  String getDefaultPlan(String planDays, String price) {
    return 'Current package ($planDays Days): $price FCFA';
  }

  @override
  String get optionTrafficInfo => 'Traffic Info';

  @override
  String get optionBrt => 'BRT';

  @override
  String get optionTer => 'TER';

  @override
  String get optionParking => 'Parking';

  @override
  String get optionEnvironment => 'Environment';
}
