import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/ForgotPasswordScreen.dart';
import 'package:iziway/HomeScreen.dart';
import 'package:iziway/LocationPermissionScreen.dart';
import 'package:iziway/OptionScreen.dart';
import 'package:iziway/SelectProfileScreen.dart';
import 'package:iziway/laguages/Languages.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'Common/ApiClient.dart';
import 'Common/screen_size_utils.dart';
import 'IntroducationScreen.dart';
import 'Model/DataModel.dart';
import 'Model/DataObjectResponse.dart';
import 'RegisterScreen.dart';
import 'app_theme.dart';
import 'package:geolocator/geolocator.dart';
import 'laguages/locale_constant.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var passwordFocus = FocusNode();

  String email = '', password = '';
  TextEditingController _controllerEmail, _controllerPassword;
  bool isVisibleLink = false;
  bool isRememberMe = false;

  Utils _utils;
  SharedPref _sharedPref;

  @override
  void didChangeDependencies() {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
    ScreenUtil.instance.init(context);
  }

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
  }

  Future<void> readData() async {
    email = await _sharedPref.readString(_sharedPref.keyEmail);
    password = await _sharedPref.readString(_sharedPref.keyPassword);
    String isValue = await _sharedPref.readString(_sharedPref.keyIsRememberMe);

    setState(() {
      _controllerEmail = new TextEditingController(text: email);
      _controllerPassword = new TextEditingController(text: password);

      if (!_utils.isValidationEmpty(isValue)) {
        if (isValue == 'true') {
          isRememberMe = true;
        } else {
          isRememberMe = false;
        }
      } else {
        isRememberMe = false;
      }
    });
  }

  Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Container(
              height: SV.setHeight(600),
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/images/ic_top_green.png',
                    fit: BoxFit.fill,
                    width: double.infinity,
                    height: SV.setHeight(500),
                  ),
                  Column(
                    children: <Widget>[
                      Expanded(
                        child: Container(),
                      ),
                      Image.asset(
                        'assets/images/app_logo.png',
                        width: SV.setHeight(250),
                        height: SV.setHeight(250),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(30),
                children: <Widget>[
                  Text(
                    /*'email'*/
                    Languages.of(context).lblEmail,
                    style: GoogleFonts.roboto(
                        fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      controller: _controllerEmail,
                      autofocus: false,
                      onChanged: (val) {
                        setState(() {
                          email = val;
                        });
                      },
                      style: GoogleFonts.roboto(
                          fontSize: 15, fontWeight: FontWeight.w400),
                      keyboardType: TextInputType.emailAddress,
                      cursorColor: AppTheme.Green,
                      textInputAction: TextInputAction.next,
                      onSubmitted: (v) {
                        FocusScope.of(context).requestFocus(passwordFocus);
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'entrer votre email'*/
                              Languages.of(context).validEnterEmail,
                          hintStyle: GoogleFonts.roboto(
                              fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    /*'Mot de passe'*/
                    Languages.of(context).lblPassword,
                    style: GoogleFonts.roboto(
                        fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      controller: _controllerPassword,
                      autofocus: false,
                      focusNode: passwordFocus,
                      onChanged: (val) {
                        setState(() {
                          password = val;
                        });
                      },
                      style: GoogleFonts.roboto(
                          fontSize: 15, fontWeight: FontWeight.w400),
                      obscureText: true,
                      cursorColor: AppTheme.Green,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'entrer votre mot de passe'*/ Languages
                                  .of(context)
                              .validEnterPassword,
                          hintStyle: GoogleFonts.roboto(
                              fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  // SizedBox(height: 5),
                  /*     GestureDetector(
                      onTap: () {
                         Navigator.push<dynamic>(
                          context,
                          MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) =>
                                ForgotPasswordScreen(),
                          ),
                        );
                      },
                      child: Text(
                        'Forgot password?',
                        textAlign: TextAlign.right,
                        style: GoogleFonts.roboto(
                            fontSize: 14, fontWeight: FontWeight.w400),
                      ),
                    ),*/
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Row(
                            children: [
                              SizedBox(width: 3),
                              GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isRememberMe = !isRememberMe;
                                    });
                                  },
                                  child: Icon(
                                    isRememberMe == true
                                        ? Icons.check_box_rounded
                                        : Icons.check_box_outline_blank_rounded,
                                    color: isRememberMe == true
                                        ? AppTheme.Green
                                        : AppTheme.Grey,
                                    size:
                                        ResponsiveFlutter.of(context).scale(23),
                                  )),
                              SizedBox(width: 6),
                              Text(
                                /*"Remember me"*/
                                Languages.of(context).hintRememberMe,
                                style: GoogleFonts.roboto(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500,
                                    color: AppTheme.Grey),
                              ),
                            ],
                          ),
                        ),
                        /*child: ListTileTheme(
                              contentPadding: EdgeInsets.all(0),
                              child: CheckboxListTile(
                                  title: Text(
                                    'Remember me',
                                    textAlign: TextAlign.start,
                                    style: GoogleFonts.roboto(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  value: isRememberMe,
                                  activeColor: AppTheme.Green,
                                  onChanged: (bool newValue) {
                                    setState(() {
                                      isRememberMe = newValue;
                                    });
                                  }),
                            )*/
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) =>
                                  ForgotPasswordScreen(),
                            ),
                          );
                        },
                        child: Text(
                          /*'Forgot Password?'*/
                          Languages.of(context).lblForgotPassword,
                          textAlign: TextAlign.right,
                          style: GoogleFonts.roboto(
                              fontSize: 13, fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: isVisibleLink,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _utils
                              .isNetworkAvailable(context,_utils,showDialog: true)
                              .then((value) => checkNetworkResend(value));
                        });
                      },
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 60),
                            Text(
                              /*'Resend Confirmation Link'*/
                              Languages.of(context).btnResendConfirmLink,
                              style: GoogleFonts.roboto(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                            SizedBox(height: 20),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 40),
                  GestureDetector(
                    onTap: () {
                      checkValidation();
                    },
                    child: Container(
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: AppTheme.Green,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'SE CONNECTER'*/
                        /*Languages.of(context).btnToLogin*/
                        /* ApplicationLocalizations.of(context)
                            .translate(Languages.of(context).btnToLogin)*/
                        Languages.of(context).btnToLogin,
                        style: GoogleFonts.roboto(
                            color: AppTheme.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  GestureDetector(
                    onTap: () {
                       Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              SelectProfileScreen(),
                        ),
                      );
                    },
                    child: Container(
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 1),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'S\'INSCRIRE'*/
                        /*Languages.of(context).btnSubscribe*/
                        /* ApplicationLocalizations.of(context)
                            .translate(Languages.of(context).btnSubscribe)*/
                        Languages.of(context).btnSubscribe,
                        style: GoogleFonts.roboto(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  GestureDetector(
                    onTap: () {
                      _utils.dialogChooseLanguage(_locale);
                    },
                    child: Container(
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 1),
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          SizedBox(width: 20),
                          Image.asset(
                            'assets/images/ic_language.png',
                            color: Colors.black,
                            width: ResponsiveFlutter.of(context).scale(24),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(24),
                          ),
                          SizedBox(width: 20),
                          Flexible(
                            flex: 1,
                            child: Container(
                              alignment: AlignmentDirectional.center,
                              child: Text(
                                Languages.of(context).btnChangeLanguage,
                                style: GoogleFonts.roboto(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                          SizedBox(width: 20),
                          SizedBox(
                            width: ResponsiveFlutter.of(context).scale(25),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(25),
                          ),
                          SizedBox(width: 20),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void checkValidation() {
    if (_utils.isValidationEmpty(email)) {
      _utils.alertDialog(Languages.of(context).enterEmail);
    } else if (!_utils.emailValidator(email)) {
      _utils.alertDialog(Languages.of(context).enterValidEmail);
    } else if (_utils.isValidationEmpty(password)) {
      _utils.alertDialog(Languages.of(context).enterPassword);
    } else {
      _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) => checkNetwork(value));
    }
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileTypeUser);
      Future<DataObjectResponse> user = login();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataObjectResponse> login() async {
    try {
      FormData formData = FormData.fromMap({
        'email': email,
        'password': _utils.generateMd5(password),
        'device_type': _utils.getDeviceType(),
        'device_token': "token"
      });
      Response response = await ApiClient()
          .apiClientInstance(context, '')
          .post(ApiClient.login, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataObjectResponse objectResponse) {
    _utils.hideProgressDialog();
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        Constant.profileType = objectResponse.data.user_type;
        storeData(objectResponse)
            .then((value) => redirectToOtherScreen(objectResponse));
      } else if (objectResponse.ResponseCode == 2) {
        _utils.alertDialog(objectResponse.ResponseMsg);
        setState(() {
          isVisibleLink = true;
        });
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  Future<void> redirectToOtherScreen(DataObjectResponse objectResponse) async {
    // check location permission
    PermissionStatus permission =
        await LocationPermissions().checkPermissionStatus();
    if (permission != PermissionStatus.granted) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LocationPermissionScreen()),
          (Route<dynamic> route) => false);
    } else {
      //check agreement accept or not
      String isAgreement =
          await _sharedPref.readString(_sharedPref.isAgreement);
      if (_utils.isValidationEmpty(isAgreement)) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => IntroductionScreen(false)),
            (Route<dynamic> route) => false);
      } else {
        /* Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => OptionScreen()),
            (Route<dynamic> route) => false);*/

        if (Constant.profileType == Constant.profileTypeUser) {
          DataModel dataModel = objectResponse.data;
          if (dataModel.isSubscribe != null &&
              dataModel.isSubscribe == Constant.isSubscription1) {
            _utils.getLocation(_utils).then((value) async {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => OptionScreen()),
                  (Route<dynamic> route) => false);
            });
          } else if (dataModel.isFreeMember != null &&
              dataModel.isFreeMember == Constant.isFreeMember2) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (context) => IntroductionScreen(false)),
                (Route<dynamic> route) => false);
          } else if (dataModel.isFreeMember != null &&
              dataModel.isFreeMember == Constant.isFreeMember1) {
            _utils.getLocation(_utils).then((value) async {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => OptionScreen()),
                  (Route<dynamic> route) => false);
            });
          }
        } else {
          _utils.getLocation(_utils).then((value) async {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => OptionScreen()),
                (Route<dynamic> route) => false);
          });
        }
      }
    }
  }

  Future<void> storeData(DataObjectResponse objectResponse) async {
    _sharedPref.saveString(_sharedPref.userType, objectResponse.data.user_type);
    _sharedPref.saveString(_sharedPref.UserId, objectResponse.data.user_id);
    _sharedPref.saveString(_sharedPref.Token, objectResponse.data.token);
    _sharedPref.saveObject(_sharedPref.UserResponse, objectResponse.data);

    if (isRememberMe) {
      _sharedPref.saveString(_sharedPref.keyEmail, objectResponse.data.email);
      _sharedPref.saveString(_sharedPref.keyPassword, password);
    } else {
      _sharedPref.saveString(_sharedPref.keyEmail, '');
      _sharedPref.saveString(_sharedPref.keyPassword, '');
    }

    _sharedPref.saveString(
        _sharedPref.keyIsRememberMe, isRememberMe.toString());
  }

  void checkNetworkResend(value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileTypeUser);
      Future<DataObjectResponse> user = resendConfirm();
      user.then((val) => manageResendConfirmResponse(val));
    }
  }

  Future<DataObjectResponse> resendConfirm() async {
    try {
      FormData formData = FormData.fromMap({
        'email': email,
      });
      Response response = await ApiClient()
          .apiClientInstance(context, '')
          .post(ApiClient.resendConfirm, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResendConfirmResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }
/*
  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      // return Future.error(Languages.of(context).msgLocationDisable);
      // _utils.alertDialog(Languages.of(context).msgLocationDisable);
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        // return Future.error('Location permissions are denied');
        // _utils.alertDialog('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      // return Future.error(
      //     Languages.of(context).msgDeniedForever);
      _utils.alertDialog(Languages.of(context).msgDeniedForever);
      return Future.error(
          Languages.of(context).msgDeniedForever);
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }*/
}
