import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/Model/DataObjectResponse.dart';
import 'package:iziway/Model/SubscriptionModel.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'Common/Constant.dart';
import 'Model/DataModel.dart';
import 'WebviewScreen.dart';
import 'app_theme.dart';
import 'Model/DataObjectResponse.dart';
import 'Common/ApiClient.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  Utils _utils;
  List<SubscriptionModel> lstModel = new List();
  int selectedPosition = -1;
  SharedPref _sharedPref;
  DataModel userData;
  String isSubscribe = Constant.isSubscription0;
  String planDays = '0';
  String price = '0';
  bool isFirstTime=true;

  // bool isUpdate=true;



  /*void _onPressedClick() {
    Navigator.of(context, rootNavigator: true).pop(); // positive button dismiss
    // click code
    Navigator.pop(context);
  }*/

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
    /*if (isSubscribe != Constant.isSubscription1) {
      setData();
    }*/
  }

  void setData() {
    if (lstModel == null) {
      lstModel = new List();
    }
    lstModel.clear();

    lstModel.add(new SubscriptionModel(
        Languages.of(context).subscriptionPlan1,
        Constant.subscriptionPlan1Price,
        Constant.subscriptionPlan1Day,
        false));
    lstModel.add(new SubscriptionModel(
        Languages.of(context).subscriptionPlan2,
        Constant.subscriptionPlan2Price,
        Constant.subscriptionPlan2Day,
        false));
    lstModel.add(new SubscriptionModel(
        Languages.of(context).subscriptionPlan3,
        Constant.subscriptionPlan3Price,
        Constant.subscriptionPlan3Day,
        false));
    lstModel.add(new SubscriptionModel(
        Languages.of(context).subscriptionPlan4,
        Constant.subscriptionPlan4Price,
        Constant.subscriptionPlan4Day,
        false));
  }

  Future<void> readData() async {
    userData = DataModel.fromJson(
        await _sharedPref.readObject(_sharedPref.UserResponse));
    setState(() {
      isSubscribe = userData.isSubscribe;
      planDays = userData.planDays;
      price = userData.price;

      if (_utils.isValidationEmpty(planDays)) {
        planDays = '0';
      }
      if (_utils.isValidationEmpty(price)) {
        price = '0';
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    Future<bool> _goToBack() async {
      Navigator.pop(context, true);
      return Future.value(true);
    }

    if(isFirstTime) {
      setData();
      isFirstTime=false;
    }

    return WillPopScope(
      onWillPop: _goToBack,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Text(
              /*'Abonnements'*/
              Languages.of(context).titleSubscriptions,
              style: GoogleFonts.roboto(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
            ),
            leading: GestureDetector(
              onTap: _goToBack,
              child: Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 25,
              ),
            ),
          ),
          body: isSubscribe == Constant.isSubscription2
              ? Wrap(
                  children: [
                    Container(
                      padding: EdgeInsets.all(
                          ResponsiveFlutter.of(context).scale(16)),
                      child: Column(
                        children: [
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.all(
                                ResponsiveFlutter.of(context).scale(16)),
                            decoration: BoxDecoration(
                              color: AppTheme.LightGrey,
                              borderRadius: BorderRadius.circular(
                                  ResponsiveFlutter.of(context).scale(6)),
                              border: Border.all(
                                color: AppTheme.DarkRed,
                                width: ResponsiveFlutter.of(context).scale(1.5),
                              ),
                            ),
                            padding: EdgeInsets.fromLTRB(
                              ResponsiveFlutter.of(context).scale(12),
                              ResponsiveFlutter.of(context).scale(32),
                              ResponsiveFlutter.of(context).scale(12),
                              ResponsiveFlutter.of(context).scale(32),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  child: Image.asset(
                                    'assets/images/ic_fail.png',
                                    width: ResponsiveFlutter.of(context)
                                        .scale(100),
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(100),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: ResponsiveFlutter.of(context)
                                          .scale(16)),
                                  alignment: AlignmentDirectional.topCenter,
                                  child: Text(
                                    Languages.of(context).msgPurchaseFailed,
                                    style: GoogleFonts.roboto(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500,
                                        color: AppTheme.LightBlack),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              : isSubscribe == Constant.isSubscription1
                  ? Wrap(
                      children: [
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.all(
                              ResponsiveFlutter.of(context).scale(16)),
                          decoration: BoxDecoration(
                            color: AppTheme.LightGrey,
                            borderRadius: BorderRadius.circular(
                                ResponsiveFlutter.of(context).scale(6)),
                            border: Border.all(
                              color: AppTheme.Green,
                              width: ResponsiveFlutter.of(context).scale(1.5),
                            ),
                          ),
                          padding: EdgeInsets.fromLTRB(
                            ResponsiveFlutter.of(context).scale(12),
                            ResponsiveFlutter.of(context).scale(32),
                            ResponsiveFlutter.of(context).scale(12),
                            ResponsiveFlutter.of(context).scale(32),
                          ),
                          child: Column(
                            children: [
                              Container(
                                child: Image.asset(
                                  'assets/images/ic_success.png',
                                  width:
                                      ResponsiveFlutter.of(context).scale(100),
                                  height: ResponsiveFlutter.of(context)
                                      .verticalScale(100),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: ResponsiveFlutter.of(context)
                                        .scale(16)),
                                alignment: AlignmentDirectional.topCenter,
                                child: Text(
                                  _utils.getSubscriptionPlan(planDays, price),
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: AppTheme.Grey),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: ResponsiveFlutter.of(context)
                                        .scale(12)),
                                alignment: AlignmentDirectional.topCenter,
                                child: Text(
                                  Languages.of(context).msgPurchaseSuccess,
                                  style: GoogleFonts.roboto(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: AppTheme.LightBlack),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  : Container(
                      padding: EdgeInsets.all(
                          ResponsiveFlutter.of(context).scale(16)),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                left: ResponsiveFlutter.of(context).scale(32),
                                right: ResponsiveFlutter.of(context).scale(32)),
                            child: Image.asset(
                              'assets/images/bg_subscription.png',
                              width: double.infinity,
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(210),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: ResponsiveFlutter.of(context).scale(12)),
                            alignment: AlignmentDirectional.topCenter,
                            child: Text(
                              /*"Go premium"*/
                              Languages.of(context).lblGoPremium,
                              style: GoogleFonts.roboto(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: AppTheme.LightBlack),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: ResponsiveFlutter.of(context).scale(3)),
                            alignment: AlignmentDirectional.topCenter,
                            child: Text(
                              /*"get unlimited access"*/
                              Languages.of(context).lblGetUnlimitedAccess,
                              style: GoogleFonts.roboto(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: AppTheme.LightBlack),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: ResponsiveFlutter.of(context).scale(10),
                                bottom:
                                    ResponsiveFlutter.of(context).scale(10)),
                            alignment: AlignmentDirectional.topCenter,
                            child: Text(
                              /*"Lorem Ipsum Is Simply Dummy Text Of The Printing."*/
                              Languages.of(context).contentSubscription,
                              style: GoogleFonts.roboto(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w500,
                                  color: AppTheme.Grey),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              itemCount: lstModel.length,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      for (int i = 0;
                                          i < lstModel.length;
                                          i++) {
                                        if (index == i) {
                                          lstModel[i].isSelected = true;
                                          selectedPosition = i;
                                        } else {
                                          lstModel[i].isSelected = false;
                                        }
                                      }
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        top: ResponsiveFlutter.of(context)
                                            .scale(6),
                                        bottom: ResponsiveFlutter.of(context)
                                            .scale(6)),
                                    width: double.infinity,
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(55),
                                    alignment: AlignmentDirectional.center,
                                    decoration: BoxDecoration(
                                      color: AppTheme.LightGrey,
                                      borderRadius: BorderRadius.circular(
                                          ResponsiveFlutter.of(context)
                                              .scale(6)),
                                      border: lstModel[index].isSelected
                                          ? Border.all(
                                              color: AppTheme.Green,
                                              width:
                                                  ResponsiveFlutter.of(context)
                                                      .scale(1.5),
                                            )
                                          : null,
                                    ),
                                    child: Text(
                                      lstModel[index].name,
                                      style: GoogleFonts.roboto(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                          color: AppTheme.LightBlack),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              if (selectedPosition >= 0) {
                                _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) {
                                  return checkNetworkPurchasePlan(
                                      value, lstModel[selectedPosition]);
                                });
                              } else {
                                _utils.alertDialog(
                                    /*'Choose subscription plan now try again...'*/
                                    Languages.of(context).validSubscriptionPlan);
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.only(
                                  left: ResponsiveFlutter.of(context).scale(32),
                                  right:
                                      ResponsiveFlutter.of(context).scale(32),
                                  top: ResponsiveFlutter.of(context).scale(10)),
                              width: double.infinity,
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(55),
                              alignment: AlignmentDirectional.center,
                              decoration: BoxDecoration(
                                  color: AppTheme.Green,
                                  borderRadius: BorderRadius.circular(
                                      ResponsiveFlutter.of(context).scale(6))),
                              child: Text(
                                /*'Purchase'*/
                                Languages.of(context).btnPurchase,
                                style: GoogleFonts.roboto(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: AppTheme.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
        ),
      ),
    );
  }

  checkNetworkPurchasePlan(bool value, SubscriptionModel model) {
    if (value) {
      _utils.showProgressDialog(Constant.profileTypeUser);
      Future<DataObjectResponse> user = doPurchasePlan(model);
      user.then((value) => manageResponsePurchasePlan(value));
    }
  }

  Future<DataObjectResponse> doPurchasePlan(SubscriptionModel model) async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'price': model.price,
        'plan_days': model.days
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.purchasePlan, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> manageResponsePurchasePlan(
      DataObjectResponse objectResponse) async {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        // : pending: next process
        String paymentUrl = objectResponse.paymentUrl;
        var result = await Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return WebViewScreen(paymentUrl);
            },
          ),
        );

        if (result != null) {
          if (result.toString() == Constant.isSubscription2) {
            if (userData != null) {
              userData.isSubscribe = Constant.isSubscription0;
              _sharedPref.saveObject(_sharedPref.UserResponse, userData);
            }
            setState(() {
              isSubscribe = Constant.isSubscription2;
            });
          }
          if (result.toString() == Constant.isSubscription1) {
            if (userData != null) {
              userData.isSubscribe = Constant.isSubscription1;
              _sharedPref.saveObject(_sharedPref.UserResponse, userData);
            }
            setState(() {
              isSubscribe = Constant.isSubscription1;
            });
          }
          if (result.toString() == Constant.isSubscription0) {
            if (userData != null) {
              userData.isSubscribe = Constant.isSubscription0;
              _sharedPref.saveObject(_sharedPref.UserResponse, userData);
            }
            setState(() {
              isSubscribe = Constant.isSubscription0;
            });

            _utils
                .isNetworkAvailable(context,_utils,showDialog: true)
                .then((value) => checkNetworkFreeMemberShipStarted(value));
          }

          /*setState(() {
            isUpdate=true;
          });*/
        }
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  checkNetworkFreeMemberShipStarted(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = freeMemberShipStarted();
      user.then((value) => manageResponseFreeMemberShipStarted(value));
    }
  }

  Future<DataObjectResponse> freeMemberShipStarted() async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'is_started': Constant.isStarted0
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.userFreeMemberShipStarted, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponseFreeMemberShipStarted(DataObjectResponse objectResponse) {
    _utils.hideProgressDialog();
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        if (userData != null) {
          userData.isSubscribe = objectResponse.data.isSubscribe;
          _sharedPref.saveObject(_sharedPref.UserResponse, userData);
        }
        setState(() {
          // isUpdate=true;
          isSubscribe = objectResponse.data.isSubscribe;
        });
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }
}
