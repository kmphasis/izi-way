import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/laguages/Languages.dart';
import 'package:location_permissions/location_permissions.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'Common/SharedPref.dart';
import 'Common/Utils.dart';
import 'Model/DataModel.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';

typedef StringValue = void Function(String);

class AddSignalScreen extends StatefulWidget {
  DataModel data;
  StringValue _callback;

  AddSignalScreen(this.data, this._callback);

  @override
  _AddSignalScreenState createState() => _AddSignalScreenState();
}

class _AddSignalScreenState extends State<AddSignalScreen> {
  Utils _utils;
  SharedPref _sharedPref;
  String type = "1";
  String isAdd = "0";
  String description = '';

  File imageOne, imageTwo, imageThree;
  MultipartFile multipartImageOne, multipartImageTwo, multipartImageThree;
  String strFileNameOne = '', strFileNameTwo = '', strFileNameThree = '';
  double lat = 0, lng = 0;
  LocationPermissionLevel _locationPermissionLevel;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    _locationPermissionLevel = LocationPermissionLevel.location;
    requestPermission(_locationPermissionLevel);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);

    Future<bool> _goToBack() async {
      Navigator.pop(context);
      widget._callback(isAdd);
      return Future.value(true);
    }

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: WillPopScope(
          onWillPop: _goToBack,
          child: Column(
            children: [
              Container(
                height: SV.setHeight(570),
                child: Stack(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/bg_agent_profile.png',
                      fit: BoxFit.fill,
                      width: double.infinity,
                      height: SV.setHeight(570),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 20, top: MediaQuery.of(context).padding.top),
                      child: Row(
                        children: [
                          SizedBox(
                            height: 40,
                            width: 40,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                                widget._callback(isAdd);
                              },
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.black,
                                size: 25,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(top: SV.setHeight(80)),
                        height: 130,
                        width: 180,
                        decoration: BoxDecoration(
                            color: AppTheme.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: SizedBox(
                          width: 100,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FadeInImage.assetNetwork(
                                  placeholder: 'assets/images/app_logo.png',
                                  image: widget.data.incident_type_image,
                                  height: 70,
                                  width: 70),
                              Text(
                                widget.data.incident_type_name,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    fontSize: 14, fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.all(20),
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: () {
                            selectImages('one');
                          },
                          child: Container(
                            height: SV.setHeight(265),
                            width: SV.setHeight(230),
                            child: Stack(
                              children: [
                                Container(
                                  height: SV.setHeight(230),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      shape: BoxShape.rectangle,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 6.0)
                                      ],
                                      image: DecorationImage(
                                          image: imageOne == null
                                              ? AssetImage(
                                                  'assets/images/ic_select_img.png')
                                              : FileImage(imageOne),
                                          fit: BoxFit.fill)),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: SV.setHeight(70),
                                          height: SV.setHeight(70),
                                          decoration: BoxDecoration(
                                              color: AppTheme.Yellow,
                                              borderRadius:
                                                  BorderRadius.circular(360)),
                                          child: Icon(
                                            Icons.add,
                                            color: AppTheme.white,
                                            size: SV.setHeight(40),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        // SizedBox(width: 15),
                        GestureDetector(
                          onTap: () {
                            selectImages('two');
                          },
                          child: Container(
                            height: SV.setHeight(265),
                            width: SV.setHeight(230),
                            child: Stack(
                              children: [
                                Container(
                                  height: SV.setHeight(230),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      shape: BoxShape.rectangle,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 6.0)
                                      ],
                                      image: DecorationImage(
                                          image: imageTwo == null
                                              ? AssetImage(
                                                  'assets/images/ic_select_img.png')
                                              : FileImage(imageTwo),
                                          fit: BoxFit.fill)),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: SV.setHeight(70),
                                          height: SV.setHeight(70),
                                          decoration: BoxDecoration(
                                              color: AppTheme.Yellow,
                                              borderRadius:
                                                  BorderRadius.circular(360)),
                                          child: Icon(
                                            Icons.add,
                                            color: AppTheme.white,
                                            size: SV.setHeight(40),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        //SizedBox(width: 15),
                        GestureDetector(
                          onTap: () {
                            selectImages('three');
                          },
                          child: Container(
                            height: SV.setHeight(265),
                            width: SV.setHeight(230),
                            child: Stack(
                              children: [
                                Container(
                                  height: SV.setHeight(230),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      shape: BoxShape.rectangle,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 6.0)
                                      ],
                                      image: DecorationImage(
                                          image: imageThree == null
                                              ? AssetImage(
                                                  'assets/images/ic_select_img.png')
                                              : FileImage(imageThree),
                                          fit: BoxFit.fill)),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: SV.setHeight(70),
                                          height: SV.setHeight(70),
                                          decoration: BoxDecoration(
                                              color: AppTheme.Yellow,
                                              borderRadius:
                                                  BorderRadius.circular(360)),
                                          child: Icon(
                                            Icons.add,
                                            color: AppTheme.white,
                                            size: SV.setHeight(40),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: AppTheme.LightGrey,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        autofocus: false,
                        onChanged: (val) {
                          setState(() {
                            description = val;
                          });
                        },
                        style: GoogleFonts.roboto(
                            fontSize: 14, fontWeight: FontWeight.w400),
                        cursorColor: AppTheme.Yellow,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 15),
                            border: InputBorder.none,
                            hintText: Languages.of(context).validEnterComment,
                            hintStyle: GoogleFonts.roboto(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                      ),
                    ),
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          type = "3";
                        });
                      },
                      child: Container(
                        height: 60,
                        margin: EdgeInsets.only(left: 20, right: 20),
                        decoration: BoxDecoration(
                            color: AppTheme.LightYellow,
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          children: [
                            Container(
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  color: AppTheme.DarkYellow,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Image.asset(
                                'assets/images/ic_signal_right.png',
                                color: AppTheme.Green,
                              ),
                            ),
                            SizedBox(width: 25),
                            Expanded(
                              child: Text(
                                Languages.of(context).textFaible,
                                style: GoogleFonts.roboto(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ),
                            Radio(
                              groupValue: type,
                              value: '3',
                              activeColor: AppTheme.LightBlack,
                              onChanged: (val) {
                                setState(() {
                                  type = "3";
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          type = "2";
                        });
                      },
                      child: Container(
                        height: 60,
                        margin: EdgeInsets.only(left: 20, right: 20),
                        decoration: BoxDecoration(
                            color: AppTheme.LightYellow,
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          children: [
                            Container(
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  color: AppTheme.DarkYellow,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Image.asset(
                                'assets/images/ic_signal_center.png',
                                color: AppTheme.Green,
                              ),
                            ),
                            SizedBox(width: 25),
                            Expanded(
                              child: Text(
                                Languages.of(context).textMoyen,
                                style: GoogleFonts.roboto(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ),
                            Radio(
                              groupValue: type,
                              value: '2',
                              activeColor: AppTheme.LightBlack,
                              onChanged: (val) {
                                setState(() {
                                  type = "2";
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          type = "1";
                        });
                      },
                      child: Container(
                        height: 60,
                        margin: EdgeInsets.only(left: 20, right: 20),
                        decoration: BoxDecoration(
                            color: AppTheme.LightYellow,
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          children: [
                            Container(
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  color: AppTheme.DarkYellow,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Image.asset(
                                'assets/images/ic_signal_left.png',
                                color: AppTheme.Green,
                              ),
                            ),
                            SizedBox(width: 25),
                            Expanded(
                              child: Text(
                                Languages.of(context).textForte,
                                style: GoogleFonts.roboto(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ),
                            Radio(
                              groupValue: type,
                              value: '1',
                              activeColor: AppTheme.LightBlack,
                              onChanged: (val) {
                                setState(() {
                                  type = "1";
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        checkValidation();
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        height: 45,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: AppTheme.Yellow,
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          /*'Envoyer'*/
                          Languages.of(context).textEnvoyer,
                          style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> requestPermission(
      LocationPermissionLevel permissionLevel) async {
    final PermissionStatus permissionRequestResult = await LocationPermissions()
        .requestPermissions(permissionLevel: permissionLevel);
    print(permissionRequestResult);
    if (permissionRequestResult == PermissionStatus.granted) {
      _determinePosition()
          .then((value) => getCurrentLatLng(value.latitude, value.longitude));
    } else if (permissionRequestResult == PermissionStatus.denied) {
      requestPermission(_locationPermissionLevel);
    } else if (permissionRequestResult == PermissionStatus.restricted ||
        permissionRequestResult == PermissionStatus.unknown) {
      await LocationPermissions().openAppSettings();
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await LocationPermissions().openAppSettings();
      return Future.error(Languages.of(context).msgLocationPermissionDisable);
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      await LocationPermissions().openAppSettings();
      return Future.error(Languages.of(context).msgDeniedForever);
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        await LocationPermissions().openAppSettings();
        // return Future.error('Location permissions are denied (actual value: $permission).'); // : pending localization
        return Future.error(Languages.of(context).getLocationPermissionDeniedWithName(permission)); // : pending localization
      }
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  void getCurrentLatLng(cLat, cLng) {
    setState(() {
      lat = cLat;
      lng = cLng;
    });
  }

  void checkValidation() {
    if (imageOne == null) {
      _utils.alertDialog(Languages.of(context).selectImage);
    } else if (_utils.isValidationEmpty(description)) {
      _utils.alertDialog(Languages.of(context).enterDescription);
    } else if (lat == 0 || lng == 0) {
      _utils.alertDialog(Languages.of(context).enableLocationPermission);
    } else {
      _utils.isNetworkAvailable( context,_utils,showDialog: true).then((value) => checkNetwork(value));
    }
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileTypeAgent);
      Future<DataObjectResponse> user = login();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataObjectResponse> login() async {
    List<MultipartFile> imageArray = [];

    if (imageOne != null) {
      imageArray.add(MultipartFile.fromFileSync(imageOne.path,
          filename: imageOne.path.split("/").last));
    }

    if (imageTwo != null) {
      imageArray.add(MultipartFile.fromFileSync(imageTwo.path,
          filename: imageTwo.path.split("/").last));
    }

    if (imageThree != null) {
      imageArray.add(MultipartFile.fromFileSync(imageThree.path,
          filename: imageThree.path.split("/").last));
    }

    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'priority': type,
        'description': description,
        'incident_type_id': widget.data.type_id,
        'lat': lat,
        'lng': lng,
        'incident_images': imageArray,
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.addIncident, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        _utils.alertDialog(objectResponse.ResponseMsg);
        setState(() {
          isAdd = "1";
          imageOne = null;
          imageTwo = null;
          imageThree = null;
          description = '';
          type = "1";
        });
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  Future<void> selectImages(String val) async {
    int wRatio = 1, hRatio = 1;

    try {
      var _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        showGif: false,
        selectCount: 1,
        showCamera: true,
        //cropConfig: CropConfig(enableCrop: true, height: hRatio, width: wRatio),
        compressSize: 500,
        uiConfig: UIConfig(
          uiThemeColor: AppTheme.Yellow,
        ),
      );
      _listImagePaths.forEach((media) {
        print(media.path.toString());
        getReturnImage(media.path.toString(), val);
      });
      setState(() {});
    } on PlatformException {}
  }

  getReturnImage(value, type) {
    if (value != null) {
      setState(() {
        if (type == "one") {
          imageOne = new File(value);
        } else if (type == "two") {
          imageTwo = new File(value);
        } else {
          imageThree = new File(value);
        }
      });
    }
  }
}
