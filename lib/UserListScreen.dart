import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'Common/screen_size_utils.dart';
import 'Model/DataArrayResponse.dart';
import 'Model/DataModel.dart';
import 'app_theme.dart';

class UserListScreen extends StatefulWidget {
  @override
  _UserListScreenState createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {

  Utils _utils;
  SharedPref _sharedPref;
  List<DataModel> agentArray = [];

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();

    _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) => checkNetwork(value));

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            /*'Equipe IZI way'*/Languages.of(context).titleEquipeIZIWay,
            style: GoogleFonts.roboto(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Colors.black),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
              size: 25,
            ),
          ),
        ),
        body: ListView.builder(
            padding: EdgeInsets.all(0),
            itemCount: agentArray.length,
            itemBuilder: (context, index) {
              return Container(
                height: 80,
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                padding: EdgeInsets.fromLTRB(15,10,15,10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0.0, 2.0),
                          blurRadius: 6.0)
                    ]),
                child: Row(
                  children: [
                    Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 3.0,
                            )
                          ],
                          image: DecorationImage(
                              image: NetworkImage(agentArray[index].profile),
                              fit: BoxFit.fill)),
                    ),
                    SizedBox(width: 15),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            agentArray[index].name,
                            style: GoogleFonts.roboto(
                                fontSize: 15, fontWeight: FontWeight.w600),
                          ),
                          SizedBox(height: 5),
                          Text(
                            agentArray[index].email,
                            style: GoogleFonts.roboto(
                                fontSize: 13, fontWeight: FontWeight.w400,
                                color: AppTheme.Grey),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileTypeAgent);
      Future<DataArrayResponse> user = getAgentList();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataArrayResponse> getAgentList() async {
    try {
      FormData formData =
      FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.getAgentList, data: formData);
      print(formData.fields.toString());
      return DataArrayResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataArrayResponse arrayResponse) {
    if (arrayResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (arrayResponse.ResponseCode == 1) {
        setState(() {
          agentArray = arrayResponse.data;
        });
      } else {
        _utils.alertDialog(arrayResponse.ResponseMsg);
      }
    }
  }

}
