import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'Common/SharedPref.dart';
import 'Common/Utils.dart';
import 'Common/screen_size_utils.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  Utils _utils;
  String emailAddress = "";
  SharedPref _sharedPref;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        backgroundColor: AppTheme.LightGrey,
        body: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          child: ListView(
            padding: EdgeInsets.fromLTRB(
              SV.setWidth(20),
              SV.setWidth(30),
              SV.setWidth(20),
              SV.setWidth(30),
            ),
            children: [
              SizedBox(height: SV.setHeight(150)),
              Image.asset(
                "assets/images/forgot_password.png",
                width: SV.setWidth(400),
                height: SV.setWidth(400),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: SV.setWidth(150),
                ),
                child: Text(
                  Languages.of(context).lblForgotPassword,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(fontSize: 25, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(height: SV.setHeight(150)),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SV.setHeight(50),
                ),
                child: Text(
                  Languages.of(context).decForgotPassword,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(fontSize: 19, fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: SV.setHeight(150), horizontal: SV.setWidth(50)),
                child: TextField(
                  autofocus: false,
                  onChanged: (val) {
                    setState(() {
                      emailAddress = val;
                    });
                  },
                  style: GoogleFonts.roboto(fontSize: 18, fontWeight: FontWeight.w400),
                  keyboardType: TextInputType.emailAddress,
                  cursorColor: AppTheme.Green,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(10)),
                      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                      isDense: true,
                      counter: SizedBox.shrink(),
                      hintText: /*'Enter your email'*/
                          Languages.of(context).validEnterEmail,
                      hintStyle: GoogleFonts.roboto(fontSize: 18, fontWeight: FontWeight.w400)),
                ),
              ),
              Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      checkValidation();
                    },
                    child: Container(
                      height: 45,
                      width: 200,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(color: AppTheme.Green, borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        Languages.of(context).btnSubmit,
                        style: GoogleFonts.roboto(color: AppTheme.white, fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Checking the mobile number validation
  void checkValidation() {
    if (_utils.isValidationEmpty(emailAddress)) {
      _utils.alertDialog(Languages.of(context).enterEmail);
    } else if (!_utils.emailValidator(emailAddress)) {
      _utils.alertDialog(Languages.of(context).enterValidEmail);
    } else {
      _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => startApiCalling(value));
    }
  }

  // Initialize login API
  void startApiCalling(isNetwork) {
    if (isNetwork) {
      _utils.showProgressDialog(Constant.profileTypeUser);
      Future<DataObjectResponse> userData = forgotPassword();
      userData.then((value) => manageResponse(value));
    }
  }

  // Request parameter of login API
  Future<DataObjectResponse> forgotPassword() async {
    try {
      Map<String, dynamic> body = new Map();
      body['email'] = emailAddress;
      Response response = await ApiClient().apiClientInstance(context, '').post(ApiClient.forgotPassword, data: new FormData.fromMap(body));
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  // Manage response of forgot Password API
  void manageResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        alertDialog(objectResponse.ResponseMsg);
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  void alertDialog(String title) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                // Languages.of(context).appName,
                Constant.appName,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              content: Text(title, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
              actions: <Widget>[
                /* FlatButton(
                  child: Text(Languages.of(context).btnOk),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),*/

                _utils.flatButton(Languages.of(context).btnOk, () {
                  Navigator.pop(context);
                  Navigator.of(context, rootNavigator: true).pop();
                }),
              ]);
        });
  }
}
