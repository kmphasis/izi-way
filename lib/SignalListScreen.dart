import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iziway/AddSignalScreen.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';

import 'Common/ApiClient.dart';
import 'Model/DataArrayResponse.dart';
import 'Model/DataModel.dart';

typedef StringValue = void Function(String);

class SignalListScreen extends StatefulWidget {
  StringValue _callback;

  SignalListScreen(this._callback);

  @override
  _SignalListScreenState createState() => _SignalListScreenState();
}

class _SignalListScreenState extends State<SignalListScreen> {
  Utils _utils;
  SharedPref _sharedPref;
  String isAdd = "0";
  List<DataModel> incidentTypeArray = [];

  @override
  void initState() {
    super.initState();

    _utils = Utils(context: context);
    _sharedPref = SharedPref();

    _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) => checkNetwork(value));
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _goToBack() async {
      Navigator.pop(context);
      widget._callback(isAdd);
      return Future.value(true);
    }

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            /*'Faire Une Alerte'*/
            Languages.of(context).titleMakeAnAlert,
            style: GoogleFonts.roboto(
                fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
              widget._callback(isAdd);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
              size: 25,
            ),
          ),
        ),
        body: WillPopScope(
          onWillPop: _goToBack,
          child: Container(
            margin: EdgeInsets.only(left: 10.0, right: 10.0),
            child: GridView.builder(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.all(0),
                itemCount: incidentTypeArray.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  /*childAspectRatio: MediaQuery.of(context).size.width /
                          (MediaQuery.of(context).size.height / 2.6)*/
                ),
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              AddSignalScreen(incidentTypeArray[index], (val) {
                            setState(() {
                              isAdd = val;
                            });
                          }),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 6.0)
                          ]),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FadeInImage.assetNetwork(
                              placeholder: 'assets/images/app_logo.png',
                              image:
                                  incidentTypeArray[index].incident_type_image,
                              height: 60,
                              width: 60),
                          SizedBox(height: 10),
                          Text(
                            incidentTypeArray[index].incident_type_name,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                fontSize: 12, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ),
      ),
    );
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileTypeAgent);
      Future<DataArrayResponse> user = getIncidentList();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataArrayResponse> getIncidentList() async {
    try {
      FormData formData =
          FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.getIncidentTypeList, data: formData);
      print(formData.fields.toString());
      return DataArrayResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataArrayResponse arrayResponse) {
    if (arrayResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (arrayResponse.ResponseCode == 1) {
        setState(() {
          incidentTypeArray = arrayResponse.data;
        });
      } else {
        _utils.alertDialog(arrayResponse.ResponseMsg);
      }
    }
  }
}
