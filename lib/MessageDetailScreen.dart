import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';

import 'Common/Constant.dart';
import 'Common/Utils.dart';
import 'Common/screen_size_utils.dart';
import 'Model/DataModel.dart';
import 'app_theme.dart';

class MessageDetailScreen extends StatefulWidget {
  DataModel data;

  MessageDetailScreen({this.data});

  @override
  _MessageDetailScreenState createState() => _MessageDetailScreenState();
}

class _MessageDetailScreenState extends State<MessageDetailScreen> {
  String type = "1";
  Utils _utils;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);

    setState(() {
      type = widget.data.priority;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);

    Future<bool> _goToBack() async {
      Navigator.pop(context);
      return Future.value(true);
    }

    return WillPopScope(
      onWillPop: _goToBack,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Column(
            children: [
              Container(
                height: SV.setHeight(570),
                child: Stack(
                  children: <Widget>[
                    Image.asset(
                      Constant.profileType == Constant.profileTypeUser
                          ? 'assets/images/bg_user_profile.png'
                          : 'assets/images/bg_agent_profile.png',
                      fit: BoxFit.fill,
                      width: double.infinity,
                      height: SV.setHeight(570),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 20, top: MediaQuery.of(context).padding.top),
                      child: Row(
                        children: [
                          SizedBox(
                            height: 40,
                            width: 40,
                            child: GestureDetector(
                              onTap: _goToBack,
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.black,
                                size: 25,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(top: SV.setHeight(80)),
                        height: 130,
                        width: 180,
                        decoration: BoxDecoration(
                            color: AppTheme.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: SizedBox(
                          width: 100,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FadeInImage.assetNetwork(
                                  placeholder: 'assets/images/app_logo.png',
                                  image: widget.data.incident_type_image,
                                  height: 70,
                                  width: 70),
                              Text(
                                widget.data.incident_type_name,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.all(20),
                  children: [
                    Container(
                      height: SV.setHeight(200),
                      child: ListView.builder(
                          itemCount: widget.data.incident_images.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                _utils.alertDialogImagePreview(
                                    widget.data.incident_images[index].image);
                              },
                              child: Container(
                                padding: EdgeInsets.all(2),
                                margin: EdgeInsets.only(left: 10, right: 10),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.black, width: 1)),
                                child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/app_logo.png',
                                    fit: BoxFit.fill,
                                    image: widget
                                        .data.incident_images[index].image,
                                    height: SV.setHeight(200),
                                    width: SV.setHeight(200)),
                              ),
                            );
                          }),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: AppTheme.LightGrey,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        widget.data.description,
                        style: GoogleFonts.roboto(
                            fontSize: 14, fontWeight: FontWeight.w400),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 60,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      decoration: BoxDecoration(
                          color: Constant.profileType == Constant.profileTypeUser
                              ? AppTheme.LightGreen
                              : AppTheme.LightYellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                color: Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.DarkGreen
                                    : AppTheme.DarkYellow,
                                borderRadius: BorderRadius.circular(10)),
                            child: Image.asset(
                              'assets/images/ic_signal_right.png',
                              color: Constant.profileType == Constant.profileTypeUser
                                  ? AppTheme.white
                                  : AppTheme.Green,
                            ),
                          ),
                          SizedBox(width: 25),
                          Expanded(
                            child: Text(
                              Languages.of(context).textFaible,
                              style: GoogleFonts.roboto(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Radio(
                            groupValue: type,
                            value: '3',
                            activeColor: AppTheme.LightBlack,
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      height: 60,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      decoration: BoxDecoration(
                          color: Constant.profileType == Constant.profileTypeUser
                              ? AppTheme.LightGreen
                              : AppTheme.LightYellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                color: Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.DarkGreen
                                    : AppTheme.DarkYellow,
                                borderRadius: BorderRadius.circular(10)),
                            child: Image.asset(
                              'assets/images/ic_signal_center.png',
                              color: Constant.profileType == Constant.profileTypeUser
                                  ? AppTheme.white
                                  : AppTheme.Green,
                            ),
                          ),
                          SizedBox(width: 25),
                          Expanded(
                            child: Text(
                              Languages.of(context).textMoyen,
                              style: GoogleFonts.roboto(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Radio(
                            groupValue: type,
                            value: '2',
                            activeColor: AppTheme.LightBlack,
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      height: 60,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      decoration: BoxDecoration(
                          color: Constant.profileType == Constant.profileTypeUser
                              ? AppTheme.LightGreen
                              : AppTheme.LightYellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                color: Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.DarkGreen
                                    : AppTheme.DarkYellow,
                                borderRadius: BorderRadius.circular(10)),
                            child: Image.asset(
                              'assets/images/ic_signal_left.png',
                              color: Constant.profileType == Constant.profileTypeUser
                                  ? AppTheme.white
                                  : AppTheme.Green,
                            ),
                          ),
                          SizedBox(width: 25),
                          Expanded(
                            child: Text(
                              Languages.of(context).textForte,
                              style: GoogleFonts.roboto(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Radio(
                            groupValue: type,
                            value: '1',
                            activeColor: AppTheme.LightBlack,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
