import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_pickers/image_pickers.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/screen_size_utils.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'Common/SharedPref.dart';
import 'Common/Utils.dart';
import 'Model/DataModel.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';

typedef StringValue = void Function(String, String);

class EditProfileScreen extends StatefulWidget {
  String temp = "";
  StringValue _callback;

  EditProfileScreen(this.temp, this._callback);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  var emailFocus = FocusNode(), mobileFocus = FocusNode();

  DataModel userData;
  SharedPref _sharedPref;
  Utils _utils;

  String profileUrl = '';
  String name = '', email = '', phoneNo = '', isNotification = '';

  File imageCropFile;
  MultipartFile profileImage;
  bool isSelectFile = false;

  String strFileName = '';

  TextEditingController _nameController = TextEditingController(),
      _phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
  }

  Future<void> readData() async {
    userData = DataModel.fromJson(
        await _sharedPref.readObject(_sharedPref.UserResponse));
    setState(() {
      name = userData.name;
      profileUrl = userData.profile;
      email = userData.email;
      phoneNo = userData.phone;
      isNotification = userData.is_noti;
      /* _nameController.value.copyWith(
          text: name, selection: TextSelection.collapsed(offset: name.length)
      );
      _phoneController.value.copyWith(
          text: phoneNo,
          selection: TextSelection.collapsed(offset: phoneNo.length));*/
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);

    Future<bool> _goToBack() async {
      Navigator.pop(context);
      widget._callback(name, profileUrl);
      return Future.value(true);
    }

    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
        body: WillPopScope(
          onWillPop: _goToBack,
          child: Column(
            children: [
              Container(
                height: SV.setHeight(650),
                child: Stack(
                  children: <Widget>[
                    Image.asset(
                      Constant.profileType == Constant.profileTypeUser
                          ? 'assets/images/bg_user_profile.png'
                          : 'assets/images/bg_agent_profile.png',
                      fit: BoxFit.fill,
                      width: double.infinity,
                      height: SV.setHeight(500),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 20, top: MediaQuery.of(context).padding.top),
                      child: Row(
                        children: [
                          SizedBox(
                            height: 40,
                            width: 40,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                                widget._callback(name, profileUrl);
                              },
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.black,
                                size: 25,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              Languages.of(context).titleEditProfile,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black),
                            ),
                          ),
                          SizedBox(width: 50),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Expanded(
                          child: Container(),
                        ),
                        GestureDetector(
                          onTap: () {
                            selectImages();
                          },
                          child: Container(
                            height: SV.setHeight(300),
                            width: SV.setHeight(250),
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: SV.setHeight(250),
                                  height: SV.setHeight(250),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black26,
                                          offset: Offset(0.0, 2.0),
                                          blurRadius: 3.0,
                                        )
                                      ],
                                      image: DecorationImage(
                                          image: imageCropFile != null
                                              ? FileImage(imageCropFile)
                                              : profileUrl != ''
                                                  ? NetworkImage(profileUrl)
                                                  : AssetImage(
                                                      'assets/images/ic_default_profile.png'),
                                          fit: BoxFit.cover)),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: SV.setHeight(90),
                                          height: SV.setHeight(90),
                                          decoration: BoxDecoration(
                                              color: Constant.profileType ==
                                                      Constant.profileTypeUser
                                                  ? AppTheme.Green
                                                  : AppTheme.Yellow,
                                              border: Border.all(
                                                  color: AppTheme.OffWhite,
                                                  width: 3),
                                              borderRadius:
                                                  BorderRadius.circular(360)),
                                          child: Icon(
                                            Icons.edit,
                                            color: AppTheme.white,
                                            size: SV.setHeight(40),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                  children: [
                    Center(
                      child: Text(
                        name,
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                    ),
                    SizedBox(height: 30),
                    Text(
                      Languages.of(context).lblLastName,
                      style: GoogleFonts.roboto(
                          fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 40,
                      child: TextField(
                        autofocus: false,
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w400),
                        cursorColor:
                            Constant.profileType == Constant.profileTypeUser
                                ? AppTheme.Green
                                : AppTheme.Yellow,
                        textInputAction: TextInputAction.next,
                        onSubmitted: (v) {
                          FocusScope.of(context).requestFocus(mobileFocus);
                        },
                        onChanged: (val) {
                          name = val;
                        },
                        controller: TextEditingController(text: name),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 10),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: AppTheme.Grey)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: AppTheme.Grey)),
                            counter: SizedBox.shrink(),
                            hintText: Languages.of(context).validEnterName,
                            hintStyle: GoogleFonts.roboto(
                                fontSize: 15, fontWeight: FontWeight.w400)),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      Languages.of(context).lblEmail,
                      style: GoogleFonts.roboto(
                          fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 40,
                      child: TextField(
                        autofocus: false,
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w400),
                        keyboardType: TextInputType.emailAddress,
                        cursorColor:
                            Constant.profileType == Constant.profileTypeUser
                                ? AppTheme.Green
                                : AppTheme.Yellow,
                        textInputAction: TextInputAction.next,
                        focusNode: emailFocus,
                        onSubmitted: (v) {
                          FocusScope.of(context).requestFocus(mobileFocus);
                        },
                        enabled: false,
                        controller: TextEditingController(text: email),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 10),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: AppTheme.Grey)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: AppTheme.Grey)),
                            counter: SizedBox.shrink(),
                            hintText: Languages.of(context).validEnterEmail,
                            hintStyle: GoogleFonts.roboto(
                                fontSize: 15, fontWeight: FontWeight.w400)),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      Languages.of(context).lblPhoneNo,
                      style: GoogleFonts.roboto(
                          fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 40,
                      child: TextField(
                        autofocus: false,
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w400),
                        keyboardType: TextInputType.number,
                        cursorColor:
                            Constant.profileType == Constant.profileTypeUser
                                ? AppTheme.Green
                                : AppTheme.Yellow,
                        textInputAction: TextInputAction.next,
                        focusNode: mobileFocus,
                        onChanged: (val) {
                          phoneNo = val;
                        },
                        controller: TextEditingController(text: phoneNo),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 10),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: AppTheme.Grey)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: AppTheme.Grey)),
                            counter: SizedBox.shrink(),
                            hintText: Languages.of(context).validEnterPhoneNo,
                            hintStyle: GoogleFonts.roboto(
                                fontSize: 15, fontWeight: FontWeight.w400)),
                      ),
                    ),
                    SizedBox(height: 40),
                    GestureDetector(
                      onTap: () {
                        _utils
                            .isNetworkAvailable(context,_utils,showDialog: true)
                            .then((value) => checkNetwork(value));
                      },
                      child: Container(
                        height: 45,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color:
                                Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.Green
                                    : AppTheme.Yellow,
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          Languages.of(context).btnRecord,
                          style: GoogleFonts.roboto(
                              color: Constant.profileType ==
                                      Constant.profileTypeUser
                                  ? AppTheme.white
                                  : Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = register();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataObjectResponse> register() async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'name': name,
        'email': email,
        'phone': phoneNo,
        'is_noti': isNotification,
        'profile': imageCropFile != null
            ? MultipartFile.fromFileSync(imageCropFile.path,
                filename: imageCropFile.path.split("/").last)
            : ''
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.updateProfile, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        setState(() {
          name = objectResponse.data.name;
          profileUrl = objectResponse.data.profile;
        });
        _utils.alertDialog(objectResponse.ResponseMsg);
        storeData(objectResponse);
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  Future<void> storeData(DataObjectResponse objectResponse) async {
    _sharedPref.saveString(_sharedPref.userType, objectResponse.data.user_type);
    _sharedPref.saveString(_sharedPref.UserId, objectResponse.data.user_id);
    _sharedPref.saveString(_sharedPref.Token, objectResponse.data.token);
    _sharedPref.saveObject(_sharedPref.UserResponse, objectResponse.data);
  }

  Future<void> selectImages() async {
    int wRatio = 1, hRatio = 1;

    try {
      var _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        showGif: false,
        selectCount: 1,
        showCamera: true,
        cropConfig: CropConfig(enableCrop: true, height: hRatio, width: wRatio),
        compressSize: 500,
        uiConfig: UIConfig(
          uiThemeColor: Constant.profileType == Constant.profileTypeUser
              ? AppTheme.Green
              : AppTheme.Yellow,
        ),
      );
      _listImagePaths.forEach((media) {
        print(media.path.toString());
        getReturnImage(media.path.toString());
      });
      setState(() {});
    } on PlatformException {}
  }

  getReturnImage(value) {
    if (value != null) {
      setState(() {
        imageCropFile = new File(value);
      });
    }
  }
}
