
import 'package:iziway/Model/DataModel.dart';
import 'package:json_annotation/json_annotation.dart';

part 'DataObjectResponse.g.dart';

@JsonSerializable()
class DataObjectResponse {

  DataObjectResponse();

  String cancelUrl;
  String returnUrl;
  String callbackUrl;
  String paymentUrl;
  num ResponseCode;
  String ResponseMsg;
  String Result;
  String ServerTime;
  DataModel data;

  factory DataObjectResponse.fromJson(Map<String, dynamic> json) =>
      _$DataObjectResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DataObjectResponseToJson(this);

}