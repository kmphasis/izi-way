import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:location_permissions/location_permissions.dart';

import 'Common/ApiClient.dart';
import 'Common/SharedPref.dart';
import 'Common/Utils.dart';
import 'HomeScreen.dart';
import 'IntroducationScreen.dart';
import 'LocationPermissionScreen.dart';
import 'LoginScreen.dart';
import 'Model/DataModel.dart';
import 'Model/DataObjectResponse.dart';
import 'laguages/locale_constant.dart';
import 'OptionScreen.dart';

class SplashScreen extends StatefulWidget {

  /*static void setLocale(BuildContext context, Locale newLocale) {
    var state = context.findAncestorStateOfType<_SplashScreenState>();
    state.setLocale(newLocale);
  }*/

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Utils _utils;
  SharedPref _sharedPref;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    new Future.delayed(new Duration(seconds: 2), () {
      readData();
    });
  }

  Future<void> readData() async {
    bool hashKey = await _sharedPref.containKey(_sharedPref.UserId);
    String userId = "";

    if (hashKey) {
      userId = await _sharedPref.readString(_sharedPref.UserId);
    }
    print(userId);

    if (_utils.isValidationEmpty(userId)) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => LoginScreen(),
        ),
      );
    } else {
      String userType = await _sharedPref.readString(_sharedPref.userType);
      Constant.profileType = userType;
      redirectToOtherScreen();
    }
  }

  Future<void> redirectToOtherScreen() async {
    // check location permission
    PermissionStatus permission =
        await LocationPermissions().checkPermissionStatus();
    if (permission != PermissionStatus.granted) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LocationPermissionScreen()),
          (Route<dynamic> route) => false);
    } else {
      //check agreement accept or not
      String isAgreement =
          await _sharedPref.readString(_sharedPref.isAgreement);
      if (_utils.isValidationEmpty(isAgreement)) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => IntroductionScreen(false)),
            (Route<dynamic> route) => false);
      } else {
        if (Constant.profileType == Constant.profileTypeUser) {
          _utils
              .isNetworkAvailable(context,_utils,showDialog: true)
              .then((value) => checkNetworkFreeMemberShipStarted(value));
        } else {
          _utils.getLocation(_utils).then((value) async {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => OptionScreen()),
                (Route<dynamic> route) => false);
          });
        }
      }
    }
  }

  /*@override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }*/

  /*Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void didChangeDependencies() async {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }*/

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Stack(
          children: [
            Image.asset(
              'assets/images/splash_bg.png',
              fit: BoxFit.fill,
              height: double.infinity,
              width: double.infinity,
            ),
            Column(
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: Image.asset(
                      'assets/images/splash_app_logo.png',
                      width: 150,
                      height: 150,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  child: Text(
                    /*'By AFRICANITY GROUP'*/
                    Languages.of(context).lblByAfricanityGroup,
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  checkNetworkFreeMemberShipStarted(bool value) {
    if (value) {
      // _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = freeMemberShipStarted();
      user.then((value) => manageResponseFreeMemberShipStarted(value));
    }
  }

  Future<DataObjectResponse> freeMemberShipStarted() async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'is_started': Constant.isStarted0
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.userFreeMemberShipStarted, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> manageResponseFreeMemberShipStarted(
      DataObjectResponse objectResponse) async {
    // _utils.hideProgressDialog();
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        DataModel dataModel = objectResponse.data;
        if (dataModel != null) {
          _sharedPref.saveObject(_sharedPref.UserResponse, dataModel);

          if (dataModel.isSubscribe != null &&
              dataModel.isSubscribe == Constant.isSubscription1) {
            _utils.getLocation(_utils).then((value) async {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => OptionScreen()),
                  (Route<dynamic> route) => false);
            });
          } else if (dataModel.isFreeMember != null &&
              dataModel.isFreeMember == Constant.isFreeMember2) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (context) => IntroductionScreen(false)),
                (Route<dynamic> route) => false);
          } else if (dataModel.isFreeMember != null &&
              dataModel.isFreeMember == Constant.isFreeMember1) {
            _utils.getLocation(_utils).then((value) async {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => OptionScreen()),
                  (Route<dynamic> route) => false);
            });
          }
        }
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }
}
