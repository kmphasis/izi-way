

import 'package:flutter/cupertino.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/laguages/LanguageEn.dart';
import 'package:iziway/laguages/LanguageFr.dart';

import 'Languages.dart';

class AppLocalizationsDelegate extends LocalizationsDelegate<Languages> {

  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      [Constant.languageCodeEn, Constant.languageCodeFr].contains(locale.languageCode);

  @override
  Future<Languages> load(Locale locale) => _load(locale);

  static Future<Languages> _load(Locale locale) async {
    switch (locale.languageCode) {
    // switch (locale_) {
      case Constant.languageCodeEn:
        return LanguageEn();
      case Constant.languageCodeFr:
        return LanguageFr();
      default:
        return LanguageFr();
    }
  }

  @override
  bool shouldReload(LocalizationsDelegate<Languages> old) => false;

}