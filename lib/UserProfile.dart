import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/EditProfile.dart';
import 'package:iziway/LoginScreen.dart';
import 'package:iziway/MessageScreen.dart';
import 'package:iziway/Model/DataModel.dart';
import 'package:iziway/SubscriptionScreen.dart';
import 'package:iziway/UserListScreen.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'Common/ApiClient.dart';
import 'Common/Utils.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';
import 'laguages/Languages.dart';
import 'laguages/locale_constant.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  DataModel userData;
  SharedPref _sharedPref;
  Utils _utils;

  String name = '', email = '', phoneNo = '';
  String url = '';
  bool isNotification = false;

  @override
  void didChangeDependencies() async {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
  }

  void _onPressedLogoutClick() {
    Navigator.of(context, rootNavigator: true).pop(); // positive button dismiss
    // click code
    _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => checkNetworkLogout(value));
  }

  Future<void> readData() async {
    userData = DataModel.fromJson(await _sharedPref.readObject(_sharedPref.UserResponse));
    setState(() {
      name = userData.name;
      email = userData.email;
      phoneNo = userData.phone;
      url = userData.profile;
      isNotification = userData.is_noti == "0" ? false : true;
    });
  }

  Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Column(
          children: [
            Container(
              height: SV.setHeight(650),
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    Constant.profileType == Constant.profileTypeUser ? 'assets/images/bg_user_profile.png' : 'assets/images/bg_agent_profile.png',
                    fit: BoxFit.fill,
                    width: double.infinity,
                    height: SV.setHeight(500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20, top: MediaQuery.of(context).padding.top),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 40,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.black,
                              size: 25,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            /*"My IZI WAY"*/
                            Languages.of(context).titleMyIziWay,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),
                          ),
                        ),
                        SizedBox(width: 50),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Expanded(
                        child: Container(),
                      ),
                      Container(
                        height: SV.setHeight(300),
                        width: SV.setHeight(250),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: SV.setHeight(250),
                              height: SV.setHeight(250),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                  image: url == "" ? DecorationImage(image: AssetImage('assets/images/ic_default_profile.png'), fit: BoxFit.cover) : DecorationImage(image: NetworkImage(url), fit: BoxFit.cover)),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        name,
                        style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
                      ),
                      SizedBox(height: 5),
                      Container(
                        width: SV.setWidth(500),
                        color: AppTheme.Grey,
                        height: 0.5,
                      ),
                      SizedBox(height: 5),
                      Text(
                        /*"Izi Way"*/
                        // Languages.of(context).appName,
                        Constant.appName,
                        style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w400, color: AppTheme.deactivatedText),
                      ),
                      SizedBox(height: 5),
                      /*Text(
                          "Points : 8 | Rejoint il y'a 1 Jour",
                          style: GoogleFonts.roboto(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: AppTheme.deactivatedText),
                        ),*/
                    ],
                  ),
                  SizedBox(height: 30),
                  GestureDetector(
                    onTap: () {
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => EditProfileScreen("", (strName, strProfile) {
                            print(strName + " -- " + strProfile);
                            setState(() {
                              name = strName;
                              url = strProfile;
                            });
                          }),
                        ),
                      );
                    },
                    child: Container(
                      height: 55,
                      decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          SizedBox(width: 20),
                          Icon(
                            Icons.person,
                            color: AppTheme.Grey,
                            size: 25,
                          ),
                          SizedBox(width: 20),
                          Text(
                            /*"Compte utilisateur"*/
                            Languages.of(context).lblUserAccount,
                            style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  GestureDetector(
                    onTap: () {
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => MessageScreen(),
                        ),
                      );
                    },
                    child: Container(
                      height: 55,
                      decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          SizedBox(width: 20),
                          Icon(
                            Icons.mail,
                            color: AppTheme.Grey,
                            size: 25,
                          ),
                          SizedBox(width: 20),
                          Text(
                            /*"messagerie"*/
                            Languages.of(context).btnMessaging,
                            style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: Constant.profileType == Constant.profileTypeUser ? true : false,
                    child: Column(
                      children: [
                        SizedBox(height: 15),
                        GestureDetector(
                          onTap: () {

                              Navigator.push<dynamic>(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) => SubscriptionScreen(),
                                ),
                              );

                          },
                          child: Container(
                            height: 55,
                            decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                SizedBox(width: 20),
                                Image.asset(
                                  'assets/images/ic_subscription.png',
                                  color: AppTheme.Grey,
                                  width: ResponsiveFlutter.of(context).scale(25),
                                  height: ResponsiveFlutter.of(context).verticalScale(25),
                                ),
                                SizedBox(width: 20),
                                Text(
                                  /*"Abonnements"*/
                                  Languages.of(context).titleSubscriptions,
                                  style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: Constant.profileType == Constant.profileTypeUser ? false : true,
                    child: Column(
                      children: [
                        SizedBox(height: 15),
                        GestureDetector(
                          onTap: () {
                            Navigator.push<dynamic>(
                              context,
                              MaterialPageRoute<dynamic>(
                                builder: (BuildContext context) => UserListScreen(),
                              ),
                            );
                          },
                          child: Container(
                            height: 55,
                            decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                SizedBox(width: 20),
                                Icon(
                                  Icons.group,
                                  color: AppTheme.Grey,
                                  size: 25,
                                ),
                                SizedBox(width: 20),
                                Text(
                                  /*"Equipe IZI way"*/
                                  Languages.of(context).titleEquipeIZIWay,
                                  style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    height: 55,
                    decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      children: [
                        SizedBox(width: 20),
                        Icon(Icons.settings, color: AppTheme.Grey, size: 25),
                        SizedBox(width: 20),
                        Expanded(
                          child: Text(
                            /*"Reglages"*/
                            Languages.of(context).btnSettings,
                            style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                          ),
                        ),
                        Switch(
                          //width: 45.0,
                          //height: 20.0,
                          //toggleSize: 15.0,
                          value: isNotification,
                          //toggleColor: AppTheme.white,
                          activeColor: AppTheme.LightBlack,
                          //showOnOff: true,
                          //valueFontSize: 8,
                          onChanged: (val) {
                            setState(() {
                              isNotification = val;
                              _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => checkNetwork(value));
                            });
                          },
                        ),
                        SizedBox(width: 20),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  GestureDetector(
                    onTap: () {
                      _utils.dialogChooseLanguage(_locale);
                    },
                    child: Container(
                      height: 55,
                      decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          SizedBox(width: 20),
                          Image.asset(
                            'assets/images/ic_language.png',
                            color: AppTheme.Grey,
                            width: ResponsiveFlutter.of(context).scale(25),
                            height: ResponsiveFlutter.of(context).verticalScale(25),
                          ),
                          SizedBox(width: 20),
                          Text(
                            Languages.of(context).btnChangeLanguage,
                            style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  GestureDetector(
                    onTap: () {
                      _utils.alertDialogWithYesNoClickHandle(Languages.of(context).confirmMsgLogout, _onPressedLogoutClick);
                    },
                    child: Container(
                      height: 55,
                      decoration: BoxDecoration(color: AppTheme.LightGrey, borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          SizedBox(width: 20),
                          Image.asset(
                            'assets/images/ic_logout.png',
                            color: AppTheme.Grey,
                            width: ResponsiveFlutter.of(context).scale(25),
                            height: ResponsiveFlutter.of(context).verticalScale(25),
                          ),
                          SizedBox(width: 20),
                          Text(
                            Languages.of(context).btnLogout,
                            style: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w600, color: AppTheme.LightBlack),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  checkNetwork(bool value) {
    if (value) {
      Future<DataObjectResponse> user = register();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataObjectResponse> register() async {
    try {
      FormData formData = FormData.fromMap({'user_id': await _sharedPref.getUserId(), 'name': name, 'email': email, 'phone': phoneNo, 'is_noti': isNotification == true ? "1" : "0", 'profile': ''});
      Response response = await ApiClient().apiClientInstance(context, await _sharedPref.getToken()).post(ApiClient.updateProfile, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        //_utils.alertDialog(objectResponse.ResponseMsg);
        storeData(objectResponse);
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  Future<void> storeData(DataObjectResponse objectResponse) async {
    _sharedPref.saveString(_sharedPref.userType, objectResponse.data.user_type);
    _sharedPref.saveString(_sharedPref.UserId, objectResponse.data.user_id);
    _sharedPref.saveString(_sharedPref.Token, objectResponse.data.token);
    _sharedPref.saveObject(_sharedPref.UserResponse, objectResponse.data);
  }

  checkNetworkLogout(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = logout();
      user.then((value) => manageResponseLogout(value));
    }
  }

  Future<DataObjectResponse> logout() async {
    try {
      FormData formData = FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient().apiClientInstance(context, await _sharedPref.getToken()).post(ApiClient.logout, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponseLogout(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        _sharedPref.removeLoginData().then((value) {
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginScreen()), (Route<dynamic> route) => false);
        });
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }
}
