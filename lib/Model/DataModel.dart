import 'package:json_annotation/json_annotation.dart';

part 'DataModel.g.dart';

@JsonSerializable()
class DataModel {

  DataModel();

  String user_id,name,email,phone,profile,user_type,agent_code,created_date,device_type,device_token,is_noti,token;
  String incident_id,agent_id,priority,description,incident_type_id,updated_date,incident_type_image,incidentTypeImageRound,incident_type_name,agentName;
  List<DataModel> incident_images;
  String type_id,image_id,image;
  String lat,lng,isSubscribe='0',planDays,price,isFreeMember='0';


  factory DataModel.fromJson(Map<String, dynamic> json) =>
      _$DataModelFromJson(json);

  Map<String, dynamic> toJson() => _$DataModelToJson(this);

  static Map<String, dynamic> toMap(DataModel location) => _$DataModelToJson(location);

}