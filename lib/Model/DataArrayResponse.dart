import 'package:json_annotation/json_annotation.dart';

import 'DataModel.dart';

part 'DataArrayResponse.g.dart';

@JsonSerializable()
class DataArrayResponse {

  DataArrayResponse();

  num ResponseCode;
  String ResponseMsg;
  String Result;
  String ServerTime;
  List<DataModel> data;
  DataModel userDetail;

  factory DataArrayResponse.fromJson(Map<String, dynamic> json) =>
      _$DataArrayResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DataArrayResponseToJson(this);

}