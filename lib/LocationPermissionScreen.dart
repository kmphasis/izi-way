import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/IntroducationScreen.dart';
import 'package:location_permissions/location_permissions.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'HomeScreen.dart';
import 'Model/DataModel.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';
import 'OptionScreen.dart';

class LocationPermissionScreen extends StatefulWidget {
  @override
  _LocationPermissionScreenState createState() =>
      _LocationPermissionScreenState();
}

class _LocationPermissionScreenState extends State<LocationPermissionScreen> {
  LocationPermissionLevel _locationPermissionLevel;
  DataModel userData;
  SharedPref _sharedPref;
  Utils _utils;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
    _locationPermissionLevel = LocationPermissionLevel.location;
  }

  Future<void> readData() async {
    userData = DataModel.fromJson(
        await _sharedPref.readObject(_sharedPref.UserResponse));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    ScreenUtil.instance.init(context);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            /*'Acces Localisation'*/
            Languages.of(context).titleAccessLocalization,
            style: GoogleFonts.roboto(
                fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          /*leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 25,
              ),
            ),*/
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Center(
                child: Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Image.asset(
                    'assets/images/ic_location.png',
                    width: SV.setHeight(250),
                    height: SV.setHeight(250),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      width: SV.setWidth(800),
                      child: Text(
                        /*'IZI WAY a besoin de la localisation\npour pourvoir fonctionner\nnormalement'*/
                        Languages.of(context).msgLocationPermission1,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(height: SV.setHeight(100)),
                  GestureDetector(
                    onTap: () async {
                      /*Navigator.push<dynamic>(
                          context,
                          MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) => IntroductionScreen(),
                          ),
                        );*/

                      // requestPermission(_locationPermissionLevel);
                      // : check: above below
                      _utils.getLocation(_utils).then((value) async {
                        requestPermission(_locationPermissionLevel);
                      });
                    },
                    child: Container(
                      height: 45,
                      width: SV.setWidth(800),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color:
                              Constant.profileType == Constant.profileTypeUser
                                  ? AppTheme.Green
                                  : AppTheme.Yellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*      'Parametres'*/
                        Languages.of(context).btnParametres,
                        style: GoogleFonts.roboto(
                            color:
                                Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.white
                                    : Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> requestPermission(
      LocationPermissionLevel permissionLevel) async {
    final PermissionStatus permissionRequestResult = await LocationPermissions()
        .requestPermissions(permissionLevel: permissionLevel);
    print(permissionRequestResult);
    if (permissionRequestResult == PermissionStatus.granted) {
      String isAgreement =
          await _sharedPref.readString(_sharedPref.isAgreement);
      if (_utils.isValidationEmpty(isAgreement)) {
        if (userData.isSubscribe == Constant.isSubscription1) {
          storeData().then((value) => redirectToOther());
        } else {
          Navigator.push<dynamic>(
            context,
            MaterialPageRoute<dynamic>(
              builder: (BuildContext context) => IntroductionScreen(true),
            ),
          );
        }
      } else {
        /* Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => OptionScreen()),
            (Route<dynamic> route) => false);*/

        if (Constant.profileType == Constant.profileTypeUser) {
          _utils
              .isNetworkAvailable(context,_utils,showDialog: true)
              .then((value) => checkNetworkFreeMemberShipStarted(value));
        } else {
          _utils.getLocation(_utils).then((value) async {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => OptionScreen()),
                (Route<dynamic> route) => false);
          });
        }
      }
    } else if (permissionRequestResult == PermissionStatus.denied) {
      requestPermission(_locationPermissionLevel);
    } else if (permissionRequestResult == PermissionStatus.restricted ||
        permissionRequestResult == PermissionStatus.unknown) {
      await LocationPermissions().openAppSettings();
    }
  }

  checkNetworkFreeMemberShipStarted(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = freeMemberShipStarted();
      user.then((value) => manageResponseFreeMemberShipStarted(value));
    }
  }

  Future<DataObjectResponse> freeMemberShipStarted() async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'is_started': Constant.isStarted0
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.userFreeMemberShipStarted, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> manageResponseFreeMemberShipStarted(
      DataObjectResponse objectResponse) async {
    _utils.hideProgressDialog();
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        DataModel dataModel = objectResponse.data;
        if (dataModel != null) {
          _sharedPref.saveObject(_sharedPref.UserResponse, dataModel);

          if (dataModel.isSubscribe != null &&
              dataModel.isSubscribe == Constant.isSubscription1) {
            _utils.getLocation(_utils).then((value) async {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => OptionScreen()),
                  (Route<dynamic> route) => false);
            });
          } else if (dataModel.isFreeMember != null &&
              dataModel.isFreeMember == Constant.isFreeMember2) {
            if (dataModel.isSubscribe == Constant.isSubscription1) {
              storeData().then((value) => redirectToOther());
            } else {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) {
                return IntroductionScreen(false);
              }), (Route<dynamic> route) => false);
            }
          } else if (dataModel.isFreeMember != null &&
              dataModel.isFreeMember == Constant.isFreeMember1) {
            _utils.getLocation(_utils).then((value) async {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => OptionScreen()),
                  (Route<dynamic> route) => false);
            });
          }
        }
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  void redirectToOther() {
    _utils.getLocation(_utils).then((value) async {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => OptionScreen()),
          (Route<dynamic> route) => false);
    });
  }

  Future<void> storeData() async {
    _sharedPref.saveString(_sharedPref.isAgreement, Constant.isAgreement1);
  }
}
