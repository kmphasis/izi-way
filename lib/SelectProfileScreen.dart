import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/RegisterScreen.dart';
import 'package:iziway/app_theme.dart';

import 'Common/screen_size_utils.dart';

class SelectProfileScreen extends StatefulWidget {
  @override
  _SelectProfileScreenState createState() => _SelectProfileScreenState();
}

class _SelectProfileScreenState extends State<SelectProfileScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    ScreenUtil.instance.init(context);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).padding.top),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: SizedBox(
                height: 40,
                width: 40,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 25,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Center(
                child: Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Image.asset(
                    'assets/images/ic_profile_img.png',
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: SV.setHeight(500),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text(
                      /*'S\'inscrire en tant que'*/Languages.of(context).lblRegisterAs,
                      style: GoogleFonts.roboto(
                          fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(height: SV.setHeight(100)),
                  GestureDetector(
                    onTap: (){
                      Constant.profileType = Constant.profileTypeUser;
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => RegisterScreen(),
                        ),
                      );
                    },
                    child: Container(
                      height: 45,
                      width: SV.setWidth(800),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: AppTheme.Green,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'Utilisateur'*/Languages.of(context).lblUser,
                        style: GoogleFonts.roboto(
                            color: AppTheme.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(height: SV.setHeight(30)),
                  GestureDetector(
                    onTap: (){
                      Constant.profileType = Constant.profileTypeAgent;
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => RegisterScreen(),
                        ),
                      );
                    },
                    child: Container(
                      height: 45,
                      width: SV.setWidth(800),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: AppTheme.Yellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'Agent eclaireur'*/Languages.of(context).lblAgentEclaireur,
                        style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
