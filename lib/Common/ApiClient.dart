import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:iziway/laguages/Languages.dart';
import 'package:iziway/laguages/locale_constant.dart';

import 'Utils.dart';

class ApiClient {
  Dio dio;
  // String baseUrl = 'http://appkiduniya.in/IZI_WAY/Api/';
  String baseUrl = 'https://client.appmania.co.in/IZI_WAY/Api/';

  static String signUp = 'User/signUp';
  static String login = 'User/login';
  static String logout = 'User/logOut';
  static String resendConfirm = 'User/resendConfirm';
  static String forgotPassword = 'User/forgotPassword';
  static String updateProfile = 'User/updateProfile';
  static String userFreeMemberShipStarted = 'User/freeMemberShipStarted';
  static String getIncidentsList = 'Agent/getIncidentsList';
  static String getIncidentTypeList = 'Agent/getIncidentTypeList';
  static String addIncident = 'Agent/addIncident';
  static String getAgentList = 'Agent/getAgentList';
  static String purchasePlan = 'Agent/purchasePlan';

  /*// Setup calling of API
  Dio apiClientInstance(context, token) {
    Utils utils = Utils(context: context);
    SharedPref sharedPref = SharedPref();

    BaseOptions options = new BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 300000,
      receiveTimeout: 60000,
    );

    dio = new Dio(options);

    print("Token : " + token);

    String languageCode = '';
    getLocale().then((locale) {
      languageCode = locale.languageCode;
    });


    dio.interceptors.add(LogInterceptor(responseBody: true));
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions option) async {
      print('language_code:' + languageCode);

      var header = {
        'key': '2b223e5cee713615ha54ac203b24e9a123703411mk',
        'token': token,
        'language': languageCode
      };
      option.headers.addAll(header);
      return option;
    }, onResponse: (Response response) async {
      return response;
    }, onError: (DioError e) {
      utils.hideProgressDialog();
      utils.alertDialog(Languages.of(context).msgError);
      return e;
    }));
    return dio;
  }*/

// apiClientInstance
  Dio apiClientInstance(context, token) {
    Utils _utils = Utils();

    BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 60000,
      receiveTimeout: 60000,
    );
    // Locale myLocale = Localizations.localeOf(context);
    // String selectedLanguage = myLocale.languageCode;
    // _utils.loggerPrint("Language code: $selectedLanguage");

    Dio dio = Dio(options);

    // _utils.loggerPrint("Token : " + token);
    // loggerPrint("Token : " + token);

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };
    String languageCode = '';
    getLocale().then((locale) {
      languageCode = locale.languageCode;
    });
    dio.interceptors.add(LogInterceptor(responseBody: true));
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions option, RequestInterceptorHandler handler) async {
          var header = {
            'key': '2b223e5cee713615ha54ac203b24e9a123703411mk',
            'token': token,
            'language': languageCode,
          };
          option.headers.addAll(header);
          return handler.next(option);
        },
        onResponse: (Response response, ResponseInterceptorHandler handler) {
          return handler.next(response);
        },
        onError: (DioError e, ErrorInterceptorHandler handler) {
          // loggerPrint('Dio DEFAULT Error Message :---------------> ${e.message}');
          if (e.type == DioErrorType.other) {
            // loggerPrint('<<<<<<<-------------other Error---------->>>>>>');
          } else if (e.type == DioErrorType.connectTimeout) {
            // loggerPrint('<<<<<<<-------------CONNECT_TIMEOUT---------->>>>>>');
          } else if (e.type == DioErrorType.receiveTimeout) {
            // loggerPrint('<<<<<<<-------------RECEIVE_TIMEOUT---------->>>>>>');
          }
          if (e.response != null && e.response?.statusCode == 404) {
            // loggerPrint(e.response?.statusCode);
          } else {
            // loggerPrint(e.message);
          }
          _utils.hideProgressDialog();
          // utils.alertDialog(Strings.msgTryAgain, contextDialog: context);
          // dialogAlert(context, _utils, Strings.msgTryAgain);
          _utils.alertDialog(Languages.of(context).msgError);
          return handler.next(e);
        },
      ),
    );
    return dio;
  }
}
