import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iziway/laguages/Languages.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/Model/DataModel.dart';

class AccidentDetailDialog extends StatefulWidget {
  DataModel data;

  AccidentDetailDialog({this.data});

  @override
  _AccidentDetailDialogState createState() => _AccidentDetailDialogState();
}

class _AccidentDetailDialogState extends State<AccidentDetailDialog> {
  Utils _utils;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26,
                  offset: Offset(0.0, 2.0),
                  blurRadius: 6.0)
            ]),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                      child: FadeInImage.assetNetwork(
                          fit: BoxFit.fill,
                          placeholder: 'assets/images/app_logo.png',
                          image: widget.data.incident_type_image,
                          height: SV.setHeight(270),
                          width: SV.setHeight(270))),
                  SizedBox(height: SV.setHeight(30)),
                  Center(
                    child: Text(
                      widget.data.incident_type_name,
                      style: GoogleFonts.roboto(
                          fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    height: SV.setHeight(200),
                    child: ListView.builder(
                        itemCount: widget.data.incident_images.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              _utils.alertDialogImagePreview(
                                  widget.data.incident_images[index].image);
                            },
                            child: Container(
                              padding: EdgeInsets.all(2),
                              margin: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 1)),
                              child: FadeInImage.assetNetwork(
                                  placeholder: 'assets/images/app_logo.png',
                                  image:
                                      widget.data.incident_images[index].image,
                                  height: SV.setHeight(200),
                                  width: SV.setHeight(200)),
                            ),
                          );
                        }),
                  ),
                  SizedBox(height: 20),
                  Text(
                    widget.data.priority == "1"
                        ? Languages.of(context).textForte
                        : widget.data.priority == "2"
                            ? Languages.of(context).textMoyen
                            : Languages.of(context).textFaible,
                    style: GoogleFonts.roboto(
                        fontSize: 22, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: 10),
                  SingleChildScrollView(
                      child: Text(
                    widget.data.description,
                    maxLines: 10,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.roboto(
                        fontSize: 16, fontWeight: FontWeight.w400),
                  )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
