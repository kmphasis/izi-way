import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/MessageDetailScreen.dart';
import 'package:iziway/Model/DataModel.dart';
import 'package:iziway/SubscriptionScreen.dart';
import 'package:iziway/app_theme.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'Model/DataArrayResponse.dart';

class MessageScreen extends StatefulWidget {
  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  Utils _utils;
  SharedPref _sharedPref;
  List<DataModel> incidentArray = [];

  @override
  void initState() {
    super.initState();

    _utils = Utils(context: context);
    _sharedPref = SharedPref();

    _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) => checkNetwork(value));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            /*'Messagerie'*/
            Languages.of(context).btnMessaging,
            style: GoogleFonts.roboto(
                fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
              size: 25,
            ),
          ),
        ),
        body: ListView.builder(
            padding: EdgeInsets.all(0),
            itemCount: incidentArray.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push<dynamic>(
                    context,
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) {
                        return MessageDetailScreen(data: incidentArray[index]);
                      },
                    ),
                  );
                },
                child: Container(
                  height: 100,
                  margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 6.0)
                      ]),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 100,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FadeInImage.assetNetwork(
                                placeholder: 'assets/images/app_logo.png',
                                image: incidentArray[index].incident_type_image,
                                height: 50,
                                width: 50),
                            Container(
                              margin: EdgeInsets.only(top: ResponsiveFlutter.of(context).moderateScale(6)),
                              child: Text(
                                incidentArray[index].incident_type_name,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    fontSize: 12, fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: AlignmentDirectional.topEnd,
                              child: Text(
                                _utils.customDateTimeFormat(
                                    _utils,
                                    incidentArray[index].created_date,
                                   /* Constant
                                        .DATE_FORMAT_YYYY_MM_DD_DASH_HH_MM_SS_COLUMN,*/
                                    Constant
                                        .DATE_FORMAT_DD_MM_YYYY_DASH_HH_MM_COLUMN_AA),
                                style: GoogleFonts.roboto(
                                    fontSize: 10, fontWeight: FontWeight.w400,color: AppTheme.Grey),
                              ),
                            ),
                            SizedBox(height: 2),
                            Text(
                              incidentArray[index].priority == "1"
                                  ? Languages.of(context).textForte
                                  : incidentArray[index].priority == "2"
                                      ? Languages.of(context).textMoyen
                                      : Languages.of(context).textFaible,
                              style: GoogleFonts.roboto(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            SizedBox(height: 5),
                            Text(
                              Languages.of(context).hintBy +
                                  ' ' +
                                  incidentArray[index].agentName,
                              style: GoogleFonts.roboto(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: AppTheme.Grey),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataArrayResponse> user = getIncidentList();
      user.then((value) {
        manageResponse(value);
      });
    }
  }

  Future<DataArrayResponse> getIncidentList() async {
    try {
      FormData formData =
          FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.getIncidentsList, data: formData);
      print(formData.fields.toString());
      return DataArrayResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataArrayResponse arrayResponse) {
    if (arrayResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (arrayResponse.ResponseCode == 1) {
        setState(() {
          incidentArray = arrayResponse.data;
        });
      } else {
        _utils.alertDialog(arrayResponse.ResponseMsg);
      }
    }
  }
}
