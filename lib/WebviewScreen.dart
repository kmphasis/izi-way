import 'dart:async';
import 'dart:convert';

// import 'package:JalWatch/Model/DataObjectResponse.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/ProgressHUD.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'Common/SharedPref.dart';
import 'Common/screen_size_utils.dart';

// import 'Consumer/PaymentFailScreen.dart';
// import 'Consumer/PaymentScreen.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';
import 'laguages/Languages.dart';

// ignore: must_be_immutable
class WebViewScreen extends StatefulWidget {
  String url;

  WebViewScreen(this.url);

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  SharedPref _sharedPref;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();

    print("test_message_url:" + widget.url);
    _sharedPref = SharedPref();
  }

  void pageFinishedLoading(String url) {
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);

    Future<bool> _goToBack() async {
      Navigator.pop(context, Constant.isSubscription0);
      return Future.value(true);
    }

    final Completer<WebViewController> _controller =
        Completer<WebViewController>();

    return WillPopScope(
      onWillPop: _goToBack,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Text(
              /*'Payment'*/Languages.of(context).titlePayment,
              style: GoogleFonts.roboto(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
            ),
            leading: GestureDetector(
              onTap: _goToBack,
              child: Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 25,
              ),
            ),
          ),
          body: ProgressHUD(
            inAsyncCall: _isLoading,
            opacity: 0.0,
            valueColor: Constant.profileType == Constant.profileTypeUser
                ? AlwaysStoppedAnimation<Color>(AppTheme.Green)
                : AlwaysStoppedAnimation<Color>(AppTheme.Yellow),
            child: Column(
              children: [
                Expanded(
                  child: WebView(
                    onPageFinished: pageFinishedLoading,
                    initialUrl: widget.url,
                    javascriptMode: JavascriptMode.unrestricted,
                    onWebViewCreated: (WebViewController webViewController) {
                      _controller.complete(webViewController);
                    },
                    javascriptChannels: Set.from([
                      JavascriptChannel(
                          name: 'Print',
                          onMessageReceived: (JavascriptMessage message) {
                            print("test_message_message:" + message.message);

                            try {
                              Map<String, dynamic> data =
                                  jsonDecode(message.message);
                              print("test_message_data:" + data.toString());

                              DataObjectResponse object =
                                  DataObjectResponse.fromJson(data);
                              print("test_message_object: "+object.toString());

                              print("test_message_ResponseCode: "+object.ResponseCode.toString());

                              if (object.ResponseCode == 1) {
                                /*Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) {
                                      return PaymentScreen(
                                            isFrom: "online",
                                            paymentDetail: object.Result);
                                    },
                                  ),
                                );*/
                                // : pending: make Payment success screen



                                storeData(object).then((value) {
                                  print("test_message_isSubscribe: "+object.data.isSubscribe.toString());
                                  Navigator.pop(
                                      context, object.data.isSubscribe);
                                });
                                /* Navigator.pop(
                                      context, Constant.isSubscription1);*/
                              } else {
                                /* Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) {
                                      return PaymentFailScreen(
                                            isFrom: "online",
                                            paymentDetail: object.Result);
                                    },
                                  ),
                                );*/
                                // : pending: make Payment fail screen
                                Navigator.pop(
                                    context, Constant.isSubscription2);
                              }
                            } catch (e) {
                              print(e);
                            }
                          })
                    ]),
                    navigationDelegate: (NavigationRequest request) {
                      return NavigationDecision.navigate;
                    },
                  ),
                ),
                // SizedBox(height: 62.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> storeData(DataObjectResponse objectResponse) async {
    _sharedPref.saveObject(_sharedPref.UserResponse, objectResponse.data);
  }
}

/*class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        final WebViewController controller = snapshot.data;
        return Row(
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () async {
                if (await controller.canGoBack()) {
                  await controller.goBack();
                } else {
                  Scaffold.of(context).showSnackBar(
                    const SnackBar(content: Text("No back history item")),
                  );
                  return;
                }
              },
            ),
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios),
              onPressed: () async {
                if (await controller.canGoForward()) {
                  await controller.goForward();
                } else {
                  Scaffold.of(context).showSnackBar(
                    const SnackBar(content: Text("No forward history item")),
                  );
                  return;
                }
              },
            ),
            IconButton(
              icon: const Icon(Icons.replay),
              onPressed: () {
                controller.reload();
              },
            ),
          ],
        );
      },
    );
  }
}*/
