import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'OptionScreen.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Custom/DashedLine.dart';
import 'package:iziway/HomeScreen.dart';
import 'package:iziway/LicenceAgreeementScreen.dart';
import 'package:iziway/LoginScreen.dart';
import 'package:iziway/SubscriptionScreen.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Common/ApiClient.dart';
import 'Common/Constant.dart';
import 'Common/SharedPref.dart';
import 'Common/Utils.dart';
import 'Common/screen_size_utils.dart';
import 'Model/DataModel.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';
import 'OptionScreen.dart';

class IntroductionScreen extends StatefulWidget {
  bool isBack;

  IntroductionScreen(this.isBack);

  @override
  _IntroductionScreenState createState() => _IntroductionScreenState();
}

class _IntroductionScreenState extends State<IntroductionScreen> {
  DataModel userData;
  SharedPref _sharedPref;
  Utils _utils;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
  }

  Future<void> readData() async {
    userData = DataModel.fromJson(
        await _sharedPref.readObject(_sharedPref.UserResponse));
    setState(() {});
  }

  Future<void> _onTapNext() async {
    if (Constant.profileType == Constant.profileTypeUser) {
      if (_utils.isValidationEmpty(userData.isFreeMember) ||
          userData.isFreeMember == Constant.isFreeMember0) {
        _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) =>
            checkNetworkFreeMemberShipStarted(value, Constant.isStarted1));
      } else if (userData.isFreeMember == Constant.isFreeMember1) {
        /* Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => LicenceAgreementScreen(),
          ),
        );*/

        storeData().then((value) => redirectToOther());
      }
    } else {
      Navigator.push<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => LicenceAgreementScreen(),
        ),
      );
    }
  }

  Future<void> _onTapSubscription() async {
    if (Constant.profileType == Constant.profileTypeUser &&
            userData.isFreeMember == Constant.isFreeMember2 ||
        userData.isSubscribe == Constant.isSubscription1) {
      if (userData.isSubscribe == Constant.isSubscription1) {
        /*Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => LicenceAgreementScreen(),
          ),
        );*/

        storeData().then((value) => redirectToOther());
      } else {
        var result = await Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => SubscriptionScreen(),
          ),
        );

        if (result != null) {
          if (result == true) {
            _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) =>
                checkNetworkFreeMemberShipStarted(value, Constant.isStarted0));
          }
        }
      }
    }
  }

  void redirectToOther() {
    _utils.getLocation(_utils).then((value) async {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => OptionScreen()),
          (Route<dynamic> route) => false);
    });
  }

  Future<void> storeData() async {
    _sharedPref.saveString(_sharedPref.isAgreement, Constant.isAgreement1);
  }

  void _onPressedLogoutClick() {
    Navigator.of(context, rootNavigator: true).pop(); // positive button dismiss
    // click code
    _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) => checkNetworkLogout(value));
  }

  checkNetworkLogout(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = logout();
      user.then((value) => manageResponseLogout(value));
    }
  }

  Future<DataObjectResponse> logout() async {
    try {
      FormData formData =
          FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.logout, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponseLogout(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        _sharedPref.removeLoginData().then((value) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LoginScreen()),
              (Route<dynamic> route) => false);
        });
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    ScreenUtil.instance.init(context);

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).padding.top),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: SizedBox(
                height: 40,
                width: 40,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Visibility(
                    visible: widget.isBack ? true : false,
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                      size: 25,
                    ),
                  ),
                ),
              ),
            ),
            Center(
              child: Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Image.asset(
                  'assets/images/ic_intro.png',
                  fit: BoxFit.fill,
                  width: double.infinity,
                  height: SV.setHeight(500),
                ),
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.fromLTRB(20, 60, 20, 0),
                children: <Widget>[
                  Text(
                    /*'Conduiser avec izi way sans\ncreinte de bouchon'*/
                    Languages.of(context).introduction1,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        fontSize: 17, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: SV.setHeight(100)),
                  Text(
                    /*'Rouler bien entoures d\'eclaireurs avec IZI WAY'*/
                    Languages.of(context).introduction2,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        fontSize: 17, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: SV.setHeight(50)),
                  Text(
                    /*'Conduire tout en sachant l\'etat de la circulation en temps reel'*/
                    Languages.of(context).introduction3,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        fontSize: 17, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: SV.setHeight(50)),
                  Text(
                    /*'Evitez les indicents de la route avec le chemin le plus rapide'*/
                    Languages.of(context).introduction4,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        fontSize: 17, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(50)),
                  Visibility(
                      visible:
                          /*Constant.profileType == Constant.profileTypeUser
                          ? userData.isSubscribe == Constant.isSubscription1
                          ? true
                          : userData.isFreeMember ==
                          Constant.isFreeMember2
                          ? false
                          : true
                          : true*/
                          true,
                      child: GestureDetector(
                        onTap: _onTapNext,
                        child: Container(
                          height: 45,
                          width: SV.setWidth(800),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Constant.profileType ==
                                      Constant.profileTypeUser
                                  ? userData.isFreeMember !=
                                          Constant.isFreeMember2
                                      ? AppTheme.Green
                                      : AppTheme.LightGrey
                                  : AppTheme.Yellow,
                              borderRadius: BorderRadius.circular(10)),
                          child: Text(
                            Constant.profileType == Constant.profileTypeUser
                                ? /*'Essai Gratuit pour 7 jours'*/ Languages.of(context).btnFreeTry
                                : Languages.of(context).btnTryNow,
                            style: GoogleFonts.roboto(
                                color: Constant.profileType ==
                                            Constant.profileTypeUser &&
                                        userData.isFreeMember !=
                                            Constant.isFreeMember2
                                    ? AppTheme.white
                                    : AppTheme.LightBlack,
                                fontSize: 17,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      )),
                  /*SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10)),*/
                  Visibility(
                      visible: Constant.profileType == Constant.profileTypeUser
                          ? /*userData.isSubscribe == Constant.isSubscription1*/ true
                          /*? false
                          : true*/
                          : false,
                      child: GestureDetector(
                        onTap: _onTapSubscription,
                        child: Container(
                          margin: EdgeInsets.only(
                              top: ResponsiveFlutter.of(context).scale(10)),
                          height: 45,
                          width: SV.setWidth(800),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Constant.profileType ==
                                              Constant.profileTypeUser &&
                                          userData.isFreeMember ==
                                              Constant.isFreeMember2 ||
                                      userData.isSubscribe ==
                                          Constant.isSubscription1
                                  ? Constant.profileType ==
                                          Constant.profileTypeUser
                                      ? AppTheme.Green
                                      : AppTheme.Yellow
                                  : AppTheme.LightGrey,
                              borderRadius: BorderRadius.circular(10)),
                          child: Text(
                            Languages.of(context).btnSubscription,
                            style: GoogleFonts.roboto(
                                color: Constant.profileType ==
                                                Constant.profileTypeUser &&
                                            userData.isFreeMember ==
                                                Constant.isFreeMember2 ||
                                        userData.isSubscribe ==
                                            Constant.isSubscription1
                                    ? AppTheme.white
                                    : AppTheme.LightBlack,
                                fontSize: 17,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      )),
                  /*SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10)),*/
                  GestureDetector(
                    onTap: () {
                      _utils.alertDialogWithYesNoClickHandle(
                          Languages.of(context).confirmMsgLogout, _onPressedLogoutClick);
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ResponsiveFlutter.of(context).moderateScale(10)),
                      height: 45,
                      decoration: BoxDecoration(
                          color:
                              Constant.profileType == Constant.profileTypeUser
                                  ? AppTheme.Green
                                  : AppTheme.Yellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          SizedBox(width: 20),
                          Image.asset(
                            'assets/images/ic_logout.png',
                            color:
                                Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.white
                                    : AppTheme.LightBlack,
                            width: ResponsiveFlutter.of(context).scale(24),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(24),
                          ),
                          SizedBox(width: 20),
                          Flexible(
                            flex: 1,
                            child: Container(
                              alignment: AlignmentDirectional.center,
                              child: Text(
                                Languages.of(context).btnLogout,
                                style: GoogleFonts.roboto(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    color: Constant.profileType ==
                                            Constant.profileTypeUser
                                        ? AppTheme.white
                                        : AppTheme.LightBlack),
                              ),
                            ),
                          ),
                          SizedBox(width: 20),
                          SizedBox(
                            width: ResponsiveFlutter.of(context).scale(25),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(25),
                          ),
                          SizedBox(width: 20),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(30)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  checkNetworkFreeMemberShipStarted(bool value, String isStarted) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = freeMemberShipStarted();
      user.then(
          (value) => manageResponseFreeMemberShipStarted(value, isStarted));
    }
  }

  Future<DataObjectResponse> freeMemberShipStarted() async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'is_started': Constant.isStarted1
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.userFreeMemberShipStarted, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> manageResponseFreeMemberShipStarted(
      DataObjectResponse objectResponse, String isStarted) async {
    _utils.hideProgressDialog();
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        if (isStarted == Constant.isStarted1) {
          if (objectResponse.data.isFreeMember != Constant.isFreeMember2) {
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => LicenceAgreementScreen(),
              ),
            );
          } else if (objectResponse.data.isSubscribe ==
              Constant.isSubscription1) {
            /*Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => LicenceAgreementScreen(),
              ),
            );*/

            storeData().then((value) => redirectToOther());
          } else {
            var result = await Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => SubscriptionScreen(),
              ),
            );

            if (result != null) {
              if (result == true) {
                _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) =>
                    checkNetworkFreeMemberShipStarted(
                        value, Constant.isStarted0));
              }
            }
          }
        } else if (objectResponse.data.isSubscribe ==
            Constant.isSubscription1) {
          /*Navigator.push<dynamic>(
            context,
            MaterialPageRoute<dynamic>(
              builder: (BuildContext context) => LicenceAgreementScreen(),
            ),
          );*/
          storeData().then((value) => redirectToOther());
        }
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }
}
