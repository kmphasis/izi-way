// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DataModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataModel _$DataModelFromJson(Map<String, dynamic> json) {
  return DataModel()
    ..user_id = json['user_id'] as String
    ..name = json['name'] as String
    ..email = json['email'] as String
    ..phone = json['phone'] as String
    ..profile = json['profile'] as String
    ..user_type = json['user_type'] as String
    ..agent_code = json['agent_code'] as String
    ..created_date = json['created_date'] as String
    ..device_type = json['device_type'] as String
    ..device_token = json['device_token'] as String
    ..is_noti = json['is_noti'] as String
    ..token = json['token'] as String
    ..incident_id = json['incident_id'] as String
    ..agent_id = json['agent_id'] as String
    ..priority = json['priority'] as String
    ..description = json['description'] as String
    ..incident_type_id = json['incident_type_id'] as String
    ..updated_date = json['updated_date'] as String
    ..incident_type_image = json['incident_type_image'] as String
    ..incidentTypeImageRound = json['incident_type_image_round'] as String
    ..incident_type_name = json['incident_type_name'] as String
    ..agentName = json['agentName'] as String
    ..incident_images = (json['incident_images'] as List)
        ?.map((e) =>
            e == null ? null : DataModel.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..type_id = json['type_id'] as String
    ..image_id = json['image_id'] as String
    ..image = json['image'] as String
    ..lat = json['lat'] as String
    ..lng = json['lng'] as String
    ..isSubscribe = json['is_subscribe'] as String
    ..planDays = json['plan_days'] as String
    ..price = json['price'] as String
    ..isFreeMember = json['is_free_member'] as String;
}

Map<String, dynamic> _$DataModelToJson(DataModel instance) => <String, dynamic>{
      'user_id': instance.user_id,
      'name': instance.name,
      'email': instance.email,
      'phone': instance.phone,
      'profile': instance.profile,
      'user_type': instance.user_type,
      'agent_code': instance.agent_code,
      'created_date': instance.created_date,
      'device_type': instance.device_type,
      'device_token': instance.device_token,
      'is_noti': instance.is_noti,
      'token': instance.token,
      'incident_id': instance.incident_id,
      'agent_id': instance.agent_id,
      'priority': instance.priority,
      'description': instance.description,
      'incident_type_id': instance.incident_type_id,
      'updated_date': instance.updated_date,
      'incident_type_image': instance.incident_type_image,
      'incident_type_image_round': instance.incidentTypeImageRound,
      'incident_type_name': instance.incident_type_name,
      'agentName': instance.agentName,
      'incident_images': instance.incident_images,
      'type_id': instance.type_id,
      'image_id': instance.image_id,
      'image': instance.image,
      'lat': instance.lat,
      'lng': instance.lng,
      'is_subscribe': instance.isSubscribe,
      'plan_days': instance.planDays,
      'price': instance.price,
      'is_free_member': instance.isFreeMember
    };
