import 'package:geolocator/geolocator.dart';

import 'Languages.dart';

class LanguageFr extends Languages {
  /* @override
  String get appName => 'IZI Way';*/

  @override
  String get selectProfile => 'Veuillez choisir une photo de profil';

  @override
  String get enterName => 'Veillez saisir votre nom ';

  @override
  String get enterPhone => 'Veillez saisir votre numéro de téléphone ';

  @override
  String get enterEmail => 'Veillez saisir votre email';

  @override
  String get enterValidEmail => 'Veillez saisir une adresse e-mail correcte ';

  @override
  String get enterPassword => 'Veillez saisir votre mot de passe';

  @override
  String get enterConPassword => 'Veillez confirmer votre mot de passe';

  @override
  String get bothPasswordNotMatch =>
      'Les deux mots de passe ne correspondent pas';

  @override
  String get enterAgentId => 'Veillez saisir l’identifiant de l’agent ';

  @override
  String get selectImage => 'Veuillez sélectionner au moins une image';

  @override
  String get enterDescription => 'Veuillez entrer la description';

  @override
  String get enableLocationPermission => 'Veillez autoriser la localisation';

  @override
  String get msgError => 'Quelle chose s\'est mal passé, veillez réessayer...';

  @override
  String get msgNotAvailableInternet =>
      'Votre internet n\'est pas disponible, veuillez réessayer plus tard.';

  @override
  String get textForte => 'Forte';

  @override
  String get textMoyen => 'Moyen';

  @override
  String get textFaible => 'Faible';

  @override
  String get textEnvoyer => 'Envoyer';

  @override
  String get btnPurchase => 'S\'ABONNER';

  @override
  String get btnFreeTry => 'Essayer (7 jours)';

  @override
  String get btnTryNow => 'Essayer maintenant';

  @override
  String get btnSubscription => 'ABONNEMMENTS';

  @override
  String get btnLogout => 'Déconnexion';

  @override
  String get btnOk => 'OK';

  @override
  String get btnNo => 'Non';

  @override
  String get btnYes => 'Oui';

  @override
  String get btnRefUser => 'Refuser';

  @override
  String get btnAccept => 'Accepter';

  @override
  String get btnParametres => 'paramètres';

  @override
  String get msgLoading => 'Chargement...';

  @override
  String get msgDeniedForever =>
      'Les autorisations de localisation sont refusées de manière permanente, nous ne pouvons pas demander d\'autorisations.';

  @override
  String get validEnterComment => 'Veuillez saisir vos commentaires ici';

  @override
  String get msgLocationPermissionDisable =>
      'Les services de localisation sont désactivés.';

  @override
  String get titleEditProfile => 'Modifier le profil';

  @override
  String get lblLastName => 'Name';

  @override
  String get validEnterName => 'Entrez votre nom ';

  @override
  String get lblEmail => 'e-mail';

  @override
  String get validEnterEmail => 'Entrez votre email';

  @override
  String get lblPhoneNo => 'Numéro de téléphone';

  @override
  String get validEnterPhoneNo => 'Entrez votre numéro de téléphone';

  @override
  String get btnRecord => 'Enregistrer';

  @override
  String get lblForgotPassword => 'Mot de passe oublié?';

  @override
  String get decForgotPassword =>
      'Veuillez saisir l\'adresse électronique associée à votre courriel. Nous vous enverrons un lien pour réinitialiser votre mot de passe.';

  @override
  String get lblPassword => 'Mot de passe';

  @override
  String get validEnterPassword => 'Entrez votre mot de passe';

  @override
  String get btnSubmit => 'Envoyer';

  @override
  String get titleAccessLocalization => 'Accéder à la localisation ';

  @override
  String get hintSearchLocation => 'Rechercher un lieu';

  @override
  String get introduction1 =>
      'Conduisez avec IZI WAY sans crainte de bouchon !';

  @override
  String get introduction2 => 'Rouler bien entoures d\'eclaireurs avec IZI WAY';

  @override
  String get introduction3 =>
      'Conduire tout en sachant l\'état de la circulation en temps réel';

  @override
  String get introduction4 =>
      'Evitez les indicents de la route avec le chemin le plus rapide';

  @override
  String get confirmMsgLogout => 'Vous êtes sûr de vouloir vous déconnecter ?';

  @override
  String get titleLicenseToUse => 'Licence d\'utilisation';

  @override
  String get msgLicenseToUse =>
      // 'Lorem Ipsum est simplement un texte factice de l\'industrie de l\'impression et de la composition. Lorem Ipsum est le texte factice standard de l\'industrie depuis les années 1500, lorsqu\'un imprimeur inconnu a pris une galère de caractères et l\'a brouillé pour en faire un spécimen de type livre. C\'était dans les années 1960 avec la sortie des feuilles Letraset contenant des passages de Lorem Ipsum, et plus récemment avec des logiciels de PAO comme Aldus PageMaker incluant des versions de Lorem Ipsum.';

      /*'<p>Merci d\’utiliser IZI WAY !<br><br>'
          'Cette politique de confidentialité décrit :<br><br>'
          'Les façons dont nous collectons les données de trafic routier et pourquoi nous le faisons. Comment nous traitons ces données et les renvoyons à la communauté IZI WAY.<br><br>'
          'Les choix dont vous disposez concernant ces données trafic. La présente Politique de confidentialité s\'applique à IZI WAY, que nous appelons icicollectivement le Service. Nous pouvons périodiquement mettre à jour cette Politique deconfidentialité en publiant une nouvelle version sur <a href="https://Africanity-group.com">https://Africanity-group.com</a>.<br><br>'
          'Si nous apportons des modifications importantes, nous vous en informerons en publiant un avis dans le Service avant que la modification ne prenne effet. Votre utilisation continue du Service après la date d\'entrée en vigueur sera soumise à la nouvelle Politique deconfidentialité.<br><br>'
          'Ⓒ Juillet 2021</p>';*/

      'Merci d\’utiliser IZI WAY !\n\n'
      'Cette politique de confidentialité décrit :\n\n'
      'Les façons dont nous collectons les données de trafic routier et pourquoi nous le faisons. Comment nous traitons ces données et les renvoyons à la communauté IZI WAY.\n\n'
      'Les choix dont vous disposez concernant ces données trafic. La présente Politique de confidentialité s\'applique à IZI WAY, que nous appelons icicollectivement le Service. Nous pouvons périodiquement mettre à jour cette Politique deconfidentialité en publiant une nouvelle version sur https://Africanity-group.com.\n\n'
      'Si nous apportons des modifications importantes, nous vous en informerons en publiant un avis dans le Service avant que la modification ne prenne effet. Votre utilisation continue du Service après la date d\'entrée en vigueur sera soumise à la nouvelle Politique deconfidentialité.\n\n'
      'Ⓒ Juillet 2021';

  @override
  String get msgLicenseToUse_1 => 'Merci d\’utiliser IZI WAY !\n\n';

  @override
  String get msgLicenseToUse_2 =>
      'Cette politique de confidentialité décrit :\n\n';

  @override
  String get msgLicenseToUse_3 =>
      'Les façons dont nous collectons les données de trafic routier et pourquoi nous le faisons. Comment nous traitons ces données et les renvoyons à la communauté IZI WAY.\n\n';

  @override
  String get msgLicenseToUse_4 =>
      'Les choix dont vous disposez concernant ces données trafic. La présente Politique de confidentialité s\'applique à IZI WAY, que nous appelons icicollectivement le Service. Nous pouvons périodiquement mettre à jour cette Politique deconfidentialité en publiant une nouvelle version sur ';

  @override
  String get msgLicenseToUse_5 => 'https://Africanity-group.com';

  @override
  String get msgLicenseToUse_6 => '.\n\n';

  @override
  String get msgLicenseToUse_7 =>
      'Si nous apportons des modifications importantes, nous vous en informerons en publiant un avis dans le Service avant que la modification ne prenne effet. Votre utilisation continue du Service après la date d\'entrée en vigueur sera soumise à la nouvelle Politique deconfidentialité.\n\n';

  @override
  String get msgLicenseToUse_8 => 'Ⓒ Juillet 2021';

  @override
  String getMsgUrlLaunch(String url) {
    return 'No se pudo iniciar $url';
  }

  @override
  String get msgLocationPermission1 =>
      'IZI WAY a besoin de la localisation pour pourvoir fonctionner normalement';

  // 'IZI WAY a besoin de la localisation pour pourvoir fonctionner normalement';

  @override
  String get hintRememberMe => 'Se souvenir de moi?';

  @override
  String get btnResendConfirmLink => 'Renvoyer le lien de confirmation';

  @override
  String get btnToLogin => 'SE CONNECTER';

  @override
  String get btnSubscribe => 'S\'INSCRIRE';

  @override
  String get btnMessaging => 'Messagerie';

  @override
  String get hintBy => 'Par';

  @override
  String get hintEnterPassword => 'Entrez le mot de passe';

  @override
  String get hintEnterConfirmPassword => 'Entrez de nouveau le mot de passe ';

  @override
  String get lblConfirmPassword => 'Confirmer le mot de passe';

  @override
  String get lblAgentId => 'ID de l\'agent';

  @override
  String get hintEnterId => 'Veuillez entrer l\'ID ici';

  @override
  String get lblRegisterAs => 'S\'inscrire en tant que';

  @override
  String get lblUser => 'Utilisateur';

  @override
  String get lblAgentEclaireur => 'Agent éclaireur';

  @override
  String get titleMakeAnAlert => 'Faire Une Alerte';

  @override
  String get lblByAfricanityGroup => 'Par AFRICANITY GROUP';

  @override
  String get subscriptionPlan1 => 'Mensuel (30 Jours): 1000 FCFA';

  @override
  String get subscriptionPlan2 => 'Trimestriel (90 Jours): 2500 FCFA';

  @override
  String get subscriptionPlan3 => 'Semestriel (180 Jours): 5000 FCFA';

  @override
  String get subscriptionPlan4 => 'Annuel (365 Jours): 10000 FCFA';

  @override
  String get titleSubscriptions => 'Abonnements';

  @override
  String get msgPurchaseFailed => 'L\'achat a échoué !';

  @override
  String get msgPurchaseSuccess => 'Achat réussi';

  @override
  String get lblGoPremium => 'Passer à la version premium';

  @override
  String get lblGetUnlimitedAccess => 'Obtenir un accès illimité ';

  @override
  String get contentSubscription =>
      'Lorem Ipsum est simplement du texte factice de l\'impression.'; // TODO: pending: dummy content

  @override
  String get validSubscriptionPlan =>
      'Choisissez votre abonnement et essayez à nouveau...';

  @override
  String get titleEquipeIZIWay => 'Équipe IZI WAY';

  @override
  String get titleMyIziWay => 'My IZI WAY';

  @override
  String get lblUserAccount => 'Compte utilisateur';

  @override
  String get btnSettings => 'Réglages';

  @override
  String get titlePayment => 'Paiement';

  @override
  String get btnChangeLanguage => 'Changer de langue';

  @override
  String getLocationPermissionDeniedWithName(LocationPermission permission) {
    return 'Location permissions are denied (actual value: $permission).';
  }

  @override
  String getDefaultPlan(String planDays, String price) {
    return 'Forfait actuel ($planDays Jours): $price FCFA';
  }

  @override
  String get optionTrafficInfo => 'Infos Trafic';

  @override
  String get optionBrt => 'BRT';

  @override
  String get optionTer => 'TER';

  @override
  String get optionParking => 'Parking';

  @override
  String get optionEnvironment => 'Environment';
}
