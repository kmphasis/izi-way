import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/Dialog/AccidentDetailDialog.dart';
import 'package:iziway/SignalListScreen.dart';
import 'package:iziway/UserProfile.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'Common/ApiClient.dart';
import 'Model/DataArrayResponse.dart';
import 'Model/DataModel.dart';
import 'app_theme.dart';
import 'package:http/http.dart' as http;
import 'dart:ui' as ui;
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class HomeScreen extends PlacesAutocompleteWidget {
  HomeScreen()
      : super(
          // apiKey: "AIzaSyB4rcCXbaD4XzyHKoIuEztLOZTpTUnAo7E",
          apiKey: Constant.apiKey,
          sessionToken: Uuid().generateV4(),
        );

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends PlacesAutocompleteState {
  // Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _controller;
  LatLngBounds bounds;

  // CameraPosition _kGooglePlex;
  double currentLatitude = 0.0;
  double currentLongitude = 0.0;
  double lat = 0.0;
  double lng = 0.0;
  Utils _utils;
  SharedPref _sharedPref;

  /*= CameraPosition(
    target: LatLng(48.8566, 2.3522),
    zoom: 12,
  );*/

  GoogleMapsPlaces _places =
      // GoogleMapsPlaces(apiKey: "AIzaSyAuYRdtg3WZwG3Q00Es0dkq6JW6nD6-Z_s");
      GoogleMapsPlaces(apiKey: Constant.apiKey);
  final homeScaffoldKey = GlobalKey<ScaffoldState>();

  final Map<MarkerId, Marker> markers = {};
  String searchLocation = '', distanceKm = '', durationTime = '';
  List<DataModel> incidentArray = [];

  AdmobBannerSize bannerSize;

  // bool isLoadedBanner=false;

  // List<int> myImage;

  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  Set<Polyline> _polylines = {};
  double totalDistance;

  setPolyLines(double lat, double lng) async {
    /*String sourceLat, sourceLng;
    if (currentLat != 0) {
      sourceLat = currentLat.toString();
      sourceLng = currentLng.toString();
    } else {
      sourceLat = "37.8179";
      sourceLng = "-122.4767437";
    }*/

    // print(lat + " -- " + lng);

    /* _googleMapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
            target: LatLng(double.parse(sourceLat), double.parse(sourceLng),),
            zoom: 17.0)));*/

    // List<PointLatLng> result = await polylinePoints.getRouteBetweenCoordinates(
    //     Constant.apiKey, currentLatitude, currentLongitude, lat, lng);

    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      Constant.apiKey,
      PointLatLng(currentLatitude, currentLongitude),
      PointLatLng(lat, lng),
    );
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    // List<PointLatLng> result = await polylinePoints.getRouteBetweenCoordinates(
    //   Constant.apiKey,
    //   PointLatLng(currentLatitude, currentLongitude),
    //   PointLatLng(lat, lng),
    // );

    if (result.points.isNotEmpty) {
      polylineCoordinates.clear();

      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    if (mounted) {
      setState(() {
        // create a Polyline instance
        // with an id, an RGB color and the list of LatLng pairs

        _polylines = {};

        Polyline polyline = Polyline(polylineId: PolylineId("poly"), color: /*Color.fromARGB(255, 40, 122, 198)*/ Colors.blue, width: 3, points: polylineCoordinates);

        // add the constructed polyline as a set of points
        // to the polyline set, which will eventually
        // end up showing up on the map
        _polylines.add(polyline);

        totalDistance = _utils.getDistanceFromGPSPointsInRoute(polylineCoordinates);
      });
    }

    polylinePoints = null;
    polylinePoints = PolylinePoints();
  }

  @override
  void initState() {
    super.initState();

    bannerSize = AdmobBannerSize.FULL_BANNER;

    _utils = Utils(context: context);
    _sharedPref = SharedPref();

    // getCurrentLatLng(0.0, 0.0);

    _determinePosition().then((value) {
      currentLatitude = value.latitude;
      currentLongitude = value.longitude;

      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(currentLatitude, currentLongitude), zoom: Constant.zoomLevel),
        ),
      );

      getCurrentLatLng(currentLatitude, currentLongitude);
    });
    _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => checkNetwork(value));

    // _utils.checkResponse(Constant.apiKey, currentLatitude, currentLongitude, lat, lng);
  }

  @override
  void dispose() {
    super.dispose();

    polylinePoints = null;
    // Wakelock.disable();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    ScreenUtil.instance.init(context);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            /*"Acces Localisation"*/
            Languages.of(context).titleAccessLocalization,
            style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.push<dynamic>(
                context,
                MaterialPageRoute<dynamic>(
                  builder: (BuildContext context) => UserProfileScreen(),
                ),
              );
            },
            child: Icon(
              Icons.menu,
              color: Colors.black,
              size: 25,
            ),
          ),
        ),
        body: Column(
          children: [
            Container(
              decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Colors.black26, offset: Offset(0.0, 1.0), blurRadius: 2.0)]),
              child: Row(
                children: [
                  Flexible(
                      child: GestureDetector(
                    onTap: _handlePressButton,
                    child: TextField(
                      autofocus: false,
                      style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                      cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                      enabled: false,
                      controller: TextEditingController(text: searchLocation),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 15),
                        border: InputBorder.none,
                        hintText: /*'Search Location...'*/ Languages.of(context).hintSearchLocation,
                        hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                        prefixIcon: Icon(
                          Icons.search,
                          color: AppTheme.Grey,
                        ),
                      ),
                    ),
                  )),
                  Visibility(
                    visible: !_utils.isValidationEmpty(searchLocation) ? true : false,
                    child: IconButton(
                      icon: Icon(
                        Icons.clear,
                        color: AppTheme.Grey,
                      ),
                      onPressed: () {
                        setState(() {
                          searchLocation = "";

                          final MarkerId markerIdSearch = MarkerId(Constant.markerIdSearch);

                          final Marker markerSearch = Marker(
                            markerId: markerIdSearch,
                            position: LatLng(lat, lng),
                            infoWindow: InfoWindow(title: searchLocation),
                            visible: false,
                          );

                          markers[markerIdSearch] = markerSearch;

                          _polylines.clear();

                          lat = currentLatitude;
                          lng = currentLongitude;

                          _controller.animateCamera(
                            CameraUpdate.newCameraPosition(
                              CameraPosition(target: LatLng(currentLatitude, currentLongitude), zoom: Constant.zoomLevel),
                            ),
                          );

                          getCurrentLatLng(lat, lng);
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Stack(
                children: [
                  GoogleMap(
                    mapType: MapType.normal,
                    /*initialCameraPosition: _kGooglePlex == null
                              ? */ /*const CameraPosition(
                                  target: LatLng(48.8566, 2.3522),
                                   zoom: 3,
                                )*/ /*
                               CameraPosition(
                                  target: LatLng(lat, lng),
                                  zoom: 3,
                                )
                              : _kGooglePlex,*/

                    initialCameraPosition: /*_kGooglePlex*/ CameraPosition(
                      target: LatLng(lat, lng),
                      // zoom: Constant.zoomLevel,
                    ),
                    myLocationButtonEnabled: false,
                    zoomControlsEnabled: false,
                    polylines: _polylines,
                    onMapCreated: (GoogleMapController controller) {
                      // _controller.complete(controller);
                      _controller = controller;
                    },
                    myLocationEnabled: true,
                    markers: Set<Marker>.of(markers.values),
                  ),
                  Visibility(
                    visible: Constant.profileType == Constant.profileTypeUser ? false : true,
                    child: Positioned(
                      bottom: 50,
                      right: 20,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) => SignalListScreen((val) {
                                if (val == "1") {
                                  _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => checkNetwork(value));
                                }
                              }),
                            ),
                          );
                        },
                        child: Container(
                          width: SV.setHeight(150),
                          height: SV.setHeight(150),
                          decoration: BoxDecoration(color: AppTheme.Yellow, border: Border.all(color: AppTheme.LightBlack, width: 2), borderRadius: BorderRadius.circular(360)),
                          child: Icon(
                            Icons.notifications,
                            color: AppTheme.white,
                            size: SV.setHeight(90),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Visibility(
              visible: !_utils.isValidationEmpty(searchLocation) ? true : false,
              child: Container(
                padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(12)),
                decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Colors.black26, offset: Offset(0.0, 1.0), blurRadius: 2.0)]),
                child: Row(
                  children: [
                    Text(
                      durationTime,
                      style: GoogleFonts.roboto(fontSize: ResponsiveFlutter.of(context).fontSize(2.5), fontWeight: FontWeight.w500, color: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow),
                    ),
                    SizedBox(width: ResponsiveFlutter.of(context).scale(12)),
                    Text(
                      '(' +
                          /* _utils
                              .removePointValue(_utils, totalDistance)
                              .toString() */
                          distanceKm +
                          ')',
                      style: GoogleFonts.roboto(fontSize: ResponsiveFlutter.of(context).fontSize(2.5), fontWeight: FontWeight.w500, color: AppTheme.Grey),
                    ),
                  ],
                ),
              ),
            ),
            AdmobBanner(
              adUnitId: AdmobBanner.testAdUnitId, //getBannerAdUnitId(),
              adSize: bannerSize,
              listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                handleEvent(event, args, 'Banner');
              },
              onBannerCreated: (AdmobBannerController controller) {
                // Dispose is called automatically for you when Flutter removes the banner from the widget tree.
                // Normally you don't need to worry about disposing this yourself, it's handled.
                // If you need direct access to dispose, this is your guy!
                // controller.dispose();
              },
            ),
          ],
        ),
      ),
    );
  }

  void handleEvent(AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        print('New Admob $adType Ad loaded!');
        /*setState(() {
          isLoadedBanner = true;
        });*/
        break;
      case AdmobAdEvent.opened:
        print('Admob $adType Ad opened!');
        /*setState(() {
          isLoadedBanner = true;
        });*/
        break;
      case AdmobAdEvent.closed:
        print('Admob $adType Ad closed!');
        /*setState(() {
          isLoadedBanner = false;
        });*/
        break;
      case AdmobAdEvent.failedToLoad:
        print('Admob $adType failed to load. :(');
        /*setState(() {
          isLoadedBanner = false;
        });*/
        break;
      default:
    }
  }

  String getAppId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-5563915608679043~8584886629';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-5563915608679043~8141005823';
    }
    return null;
  }

  String getBannerAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-5563915608679043/3389589633';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-5563915608679043/7519216963';
    }
    return null;
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataArrayResponse> user = getIncidentList();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataArrayResponse> getIncidentList() async {
    try {
      FormData formData = FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient().apiClientInstance(context, await _sharedPref.getToken()).post(ApiClient.getIncidentsList, data: formData);
      print(formData.fields.toString());
      return DataArrayResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<void> manageResponse(DataArrayResponse arrayResponse) async {
    _utils.hideProgressDialog();
    if (arrayResponse.ResponseCode != null) {
      if (arrayResponse.ResponseCode == 1) {
        if (arrayResponse != null && arrayResponse.userDetail != null) {
          if (arrayResponse.userDetail.user_type != null) {
            Constant.profileType = arrayResponse.userDetail.user_type;
          }
          storeData(arrayResponse.userDetail);
        }

        incidentArray = arrayResponse.data;
        print("test_incidentArray_start");
        print(incidentArray[0].incident_id);
        print("test_incidentArray_end");

        for (int i = 0; i < incidentArray.length; i++) {
          // var markerIdVal1 = incidentArray[i].incident_id;
          // final MarkerId markerId1 = MarkerId(markerIdVal1);

          print("AA_S -- " + incidentArray[i].lat + " -- " + incidentArray[i].lng);

          // old
          /*final Marker marker = Marker(
            markerId: markerId,
            position: LatLng(double.parse(incidentArray[i].lat),
                double.parse(incidentArray[i].lng)),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    AccidentDetailDialog(data: incidentArray[i]),
              );
            },
          );*/

          // old
          /* var iconUrl =incidentArray[i].incident_type_image;
          var request = await http.get(iconUrl);
          var dataBytes = request.bodyBytes;

          final Marker marker = Marker(
            markerId: markerId,
            icon: BitmapDescriptor.fromBytes(dataBytes.buffer.asUint8List()),
            position: LatLng(double.parse(incidentArray[i].lat),
                double.parse(incidentArray[i].lng)),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    AccidentDetailDialog(data: incidentArray[i]),
              );
            },
          );*/

          /* makeReceiptImage().then((List<int> image) {
            setState(() {
              myImage = image;
            });
          });

          print('test_image: '+myImage.toString());*/

          //   old
          /*  var iconUrl = incidentArray[i].incident_type_image;
          var request = await http.get(iconUrl);
          var dataBytes = request.bodyBytes;

          final Uint8List markerIcon = await getBytesFromAsset(
              dataBytes,
              50);

          final Uint8List markerIcon1 = await getBytesFromAsset1(
              'assets/images/ic_pin.png',
              80);

          final Marker marker = Marker(
            markerId: markerId,
            icon: BitmapDescriptor.fromBytes(markerIcon1),
            position: LatLng(double.parse(incidentArray[i].lat),
                double.parse(incidentArray[i].lng)),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    AccidentDetailDialog(data: incidentArray[i]),
              );
            },
          );*/

// marker 1
          /* final Uint8List markerIcon1 =
              await getBytesFromAsset1('assets/images/ic_pin.png', 80);

          final Marker marker1 = Marker(
            markerId: markerId1,
            icon: BitmapDescriptor.fromBytes(markerIcon1),
            position: LatLng(double.parse(incidentArray[i].lat),
                double.parse(incidentArray[i].lng)),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    AccidentDetailDialog(data: incidentArray[i]),
              );
            },
          );

          setState(() {
            markers[markerId1] = marker1;
          });*/

// marker 2
          var markerIdVal2 = incidentArray[i].incident_id;
          final MarkerId markerId2 = MarkerId(markerIdVal2);

          // var iconUrl = incidentArray[i].incident_type_image;
          var iconUrl = incidentArray[i].incidentTypeImageRound;
          // var request = await http.get(iconUrl);
          http.Response request = await http.get(Uri.parse(iconUrl));
          // var request = await http.get(Uri.parse(iconUrl));
          // var request = await http.get(Uri.https(iconUrl));
          var dataBytes = request.bodyBytes;

          final Uint8List markerIcon2 = await getBytesFromAsset2(dataBytes, 80);

          final Marker marker2 = Marker(
            markerId: markerId2,
            icon: BitmapDescriptor.fromBytes(markerIcon2),
            position: LatLng(double.parse(incidentArray[i].lat), double.parse(incidentArray[i].lng)),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => AccidentDetailDialog(data: incidentArray[i]),
              );
            },
          );

          setState(() {
            markers[markerId2] = marker2;
          });
        }
      } else {
        _utils.alertDialog(arrayResponse.ResponseMsg);
      }
    }
  }

  Future<void> _handlePressButton() async {
    print("Click on search");

    Prediction p = await PlacesAutocomplete.show(
        context: context,
        // apiKey: "AIzaSyB4rcCXbaD4XzyHKoIuEztLOZTpTUnAo7E",
        apiKey: Constant.apiKey,
        onError: onError,
        mode: Mode.fullscreen);

    displayPrediction(p, homeScaffoldKey.currentState);
  }

  void onError(PlacesAutocompleteResponse response) {
    print(response.errorMessage);
    /*homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );*/
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      _utils.showProgressDialog(Constant.profileType);

      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);

      print(detail.toJson().toString() + " -- ");

      /* setState(() {
        // searchLocation = detail.result.adrAddress;
        searchLocation = detail.result.formattedAddress;
      });*/

      searchLocation = detail.result.formattedAddress;

      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      // : pending polyline start
      setPolyLines(lat, lng);
// : pending polyline end

      // search marker add start
      final MarkerId markerIdSearch = MarkerId(Constant.markerIdSearch);

      Marker markerSearch = Marker(markerId: markerIdSearch, position: LatLng(lat, lng), infoWindow: InfoWindow(title: searchLocation), visible: true);

      /*setState(() {
        markers[markerIdSearch] = markerSearch;
      });*/

      markers[markerIdSearch] = markerSearch;

// search marker add end

      /* setState(() {
        _controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(target: LatLng(lat, lng), zoom: Constant.zoomLevel),
          ),
        );
      });*/

      /*_controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: Constant.zoomLevel),
        ),
      );*/

      /*bounds = LatLngBounds(
            southwest: LatLng(currentLatitude, currentLongitude),
            northeast: LatLng(lat, lng));*/

      List<LatLng> lstLocation = [];
      lstLocation.add(new LatLng(currentLatitude, currentLongitude));
      lstLocation.add(new LatLng(lat, lng));

      // List<LatLng> lstLocation = polylineCoordinates;

      // Future<void> getCenterMap() async {
      double minLatitude = lstLocation[0].latitude, maxLatitude = lstLocation[0].latitude, minLongitude = lstLocation[0].longitude, maxLongitude = lstLocation[0].longitude;

      for (int i = 0; i < lstLocation.length; i++) {
        if (minLatitude >= lstLocation[i].latitude) {
          minLatitude = lstLocation[i].latitude;
        }
        if (minLongitude >= lstLocation[i].longitude) {
          minLongitude = lstLocation[i].longitude;
        }
        if (maxLatitude <= lstLocation[i].latitude) {
          maxLatitude = lstLocation[i].latitude;
        }
        if (maxLongitude <= lstLocation[i].longitude) {
          maxLongitude = lstLocation[i].longitude;
        }
      }

      /* googleMapController.animateCamera(CameraUpdate.newLatLngBounds(
              LatLngBounds(
                  southwest: LatLng(minLatitude, minLongitude),
                  northeast: LatLng(maxLatitude, maxLongitude)),
              100));*/
      // }

      bounds = LatLngBounds(southwest: LatLng(minLatitude, minLongitude), northeast: LatLng(maxLatitude, maxLongitude));

      getDurationAndDistance(Constant.apiKey, currentLatitude, currentLongitude, lat, lng);

      /* setState(() {
        _controller.animateCamera(
            CameraUpdate.newLatLngBounds(bounds, Constant.zoomPadding));

        */ /* _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: Constant.zoomLevel),
        ),
      );*/ /*

        // getCurrentLatLng(lat, lng);
        this.lat = lat;
        this.lng = lng;

        new Future.delayed(const Duration(milliseconds: 1500), () {
          _utils.hideProgressDialog();
        });
      });*/

      _controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, Constant.zoomPadding));

      new Future.delayed(const Duration(milliseconds: 1500), () {
        _utils.hideProgressDialog();

        setState(() {
          /* _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: Constant.zoomLevel),
        ),
      );*/

          // getCurrentLatLng(lat, lng);
          this.lat = lat;
          this.lng = lng;
        });
      });
    }
  }

  getDurationAndDistance(String googleApiKey, double originLat, double originLong, double destLat, double destLong) async {
    /*Dio dio = new Dio();
    Response response = await dio.get(
        "https://maps.googleapis.com/maps/api/directions/json?origin=21.2305574,72.901545&destination=21.2316919,72.8662349&sensor=false&units=metric&mode=driving&key=AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY");*/
    print("test_start");
    // print(response.data);
    // print("test_end");

    /*  String distance =
        json.decode(response.data)["routes"][0]["legs"]["distance"]["text"];
    print("test_distance: "+distance);

    String duration =
        json.decode(response.data)["routes"][0]["legs"]["duration"]["text"];
    print("test_duration: "+duration);*/

    // String url = "https://maps.googleapis.com/maps/api/directions/json?origin=21.2305574,72.901545&destination=21.2316919,72.8662349&sensor=false&units=metric&mode=driving&key=AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY";

    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + originLat.toString() + "," + originLong.toString() + "&destination=" + destLat.toString() + "," + destLong.toString() + "&mode=driving" + "&key=$googleApiKey";

    // var response = await http.get(url);
    http.Response response = await http.get(Uri.parse(url));
    // var response = await http.get(Uri.parse(url));
    // var response = await http.get(Uri.https(url));
    // print('test_body: ' + response.body);

    try {
      if (response?.statusCode == 200) {
        /*Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> data = map["routes"];
        print(data[0]["legs"]);
        List<dynamic> data1 = data[0]["legs"];
        print(data1[0]["distance"]);
        print("test_distance_text: "+data1[0]["distance"]["text"]);

        print(data1[0]["duration"]);
        print("test_duration_text: "+data1[0]["duration"]["text"]);

        print('test_end');*/

        Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> routes = map["routes"];
        // print(data[0]["legs"]);
        List<dynamic> legs = routes[0]["legs"];
        // print(data1[0]["distance"]);
        dynamic distance = legs[0]["distance"];
        distanceKm = distance["text"];
        // print("test_distance_text: " + distanceKm);

        // print(data1[0]["duration"]);
        dynamic duration = legs[0]["duration"];
        durationTime = duration["text"];
        // print("test_duration_text: " + durationTime);

        // print('test_end');
      }
    } catch (error) {
      // print("test_error");
      distanceKm = '';
      durationTime = '';
      throw Exception(error.toString());
    }
  }

  void getCurrentLatLng(lat, lng) {
    print(lat.toString() + " -- " + lng.toString());

    setState(() {
      this.lat = lat;
      this.lng = lng;

      /* _kGooglePlex = CameraPosition(
        target: LatLng(lat, lng),
        zoom: 5,
      );*/

      /*markers[MarkerId(lat.toString())] = Marker(
        markerId: MarkerId(lat.toString()),
        position: LatLng(lat, lng),
        //infoWindow: InfoWindow(title: 'Paris', snippet: '*'),
        onTap: () {

        },
      );*/

      /* _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: Constant.zoomLevel),
        ),
      );*/
    });
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await LocationPermissions().openAppSettings();
      return Future.error(Languages.of(context).msgLocationPermissionDisable);
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      await LocationPermissions().openAppSettings();
      return Future.error(Languages.of(context).msgDeniedForever);
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse && permission != LocationPermission.always) {
        await LocationPermissions().openAppSettings();
        // return Future.error('Location permissions are denied (actual value: $permission).'); // : pending localization
        return Future.error(Languages.of(context).getLocationPermissionDeniedWithName(permission)); // : pending localization
      }
    }

    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  Future<void> storeData(DataModel data) async {
    _sharedPref.saveString(_sharedPref.userType, data.user_type);
    _sharedPref.saveString(_sharedPref.UserId, data.user_id);
    _sharedPref.saveString(_sharedPref.Token, data.token);
    _sharedPref.saveObject(_sharedPref.UserResponse, data);
  }

  /*Future<List<int>> makeReceiptImage() async {
    // load avatar image
    ByteData imageData =
    await rootBundle.load('assets/images/ic_subscription.png');
    List<int> bytes = Uint8List.view(imageData.buffer);
    var avatarImage = Images.decodeImage(bytes);

    //load marker image
    imageData = await rootBundle.load('assets/images/ic_logout.png');
    bytes = Uint8List.view(imageData.buffer);
    var markerImage = Images.decodeImage(bytes);

    //resize the avatar image to fit inside the marker image
    avatarImage = Images.copyResize(avatarImage,
        width: markerImage.width ~/ 1.1, height: markerImage.height ~/ 1.4);

    var radius = 90;
    int originX = avatarImage.width ~/ 2, originY = avatarImage.height ~/ 2;

    //draw the avatar image cropped as a circle inside the marker image
    for (int y = -radius; y <= radius; y++)
      for (int x = -radius; x <= radius; x++)
        if (x * x + y * y <= radius * radius)
          markerImage.setPixelSafe(originX + x + 8, originY + y + 10,
              avatarImage.getPixelSafe(originX + x, originY + y));

    return Images.encodePng(markerImage);
  }*/

  Future<Uint8List> getBytesFromAsset2(var data, int width) async {
    // ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  Future<Uint8List> getBytesFromAsset1(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

/*Future<BitmapDescriptor> getMarkerIcon(String imagePath,String infoText,Color color,double rotateDegree)  {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    //size
    Size canvasSize = Size(500.0,220.0);
    Size markerSize = Size(150.0,150.0);

    // Add info text
    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    textPainter.text = TextSpan(
      text: infoText,
      style: TextStyle(fontSize: 30.0,fontWeight: FontWeight.w600, color: color),
    );
    textPainter.layout();

    final Paint infoPaint = Paint()..color = Colors.white;
    final Paint infoStrokePaint = Paint()..color = color;
    final double infoHeight = 70.0;
    final double strokeWidth = 2.0;

    final Paint markerPaint = Paint()..color = color.withOpacity(.5);
    final double shadowWidth = 30.0;

    final Paint borderPaint = Paint()..color = color..strokeWidth=2.0..style = PaintingStyle.stroke;

    final double imageOffset = shadowWidth*.5;

    canvas.translate(canvasSize.width/2, canvasSize.height/2+infoHeight/2);

    // Add shadow circle
    canvas.drawOval(Rect.fromLTWH(-markerSize.width/2, -markerSize.height/2, markerSize.width, markerSize.height), markerPaint);
    // Add border circle
    canvas.drawOval(Rect.fromLTWH(-markerSize.width/2+shadowWidth, -markerSize.height/2+shadowWidth, markerSize.width-2*shadowWidth, markerSize.height-2*shadowWidth), borderPaint);

    // Oval for the image
    Rect oval = Rect.fromLTWH(-markerSize.width/2+.5* shadowWidth, -markerSize.height/2+.5*shadowWidth, markerSize.width-shadowWidth, markerSize.height-shadowWidth);

    //save canvas before rotate
    canvas.save();

    double rotateRadian = (pi/180.0)*rotateDegree;

    //Rotate Image
    canvas.rotate(rotateRadian);

    // Add path for oval image
    canvas.clipPath(Path()
      ..addOval(oval));

    // Add image
    ui.Image image = await getImageFromPath(imagePath);
    paintImage(canvas: canvas,image: image, rect: oval, fit: BoxFit.fitHeight);

    canvas.restore();

    // Add info box stroke
    canvas.drawPath(Path()..addRRect(RRect.fromLTRBR(-textPainter.width/2-infoHeight/2, -canvasSize.height/2-infoHeight/2+1, textPainter.width/2+infoHeight/2, -canvasSize.height/2+infoHeight/2+1,Radius.circular(35.0)))
      ..moveTo(-15, -canvasSize.height/2+infoHeight/2+1)
      ..lineTo(0, -canvasSize.height/2+infoHeight/2+25)
      ..lineTo(15, -canvasSize.height/2+infoHeight/2+1)
        , infoStrokePaint);

    //info info box
    canvas.drawPath(Path()..addRRect(RRect.fromLTRBR(-textPainter.width/2-infoHeight/2+strokeWidth, -canvasSize.height/2-infoHeight/2+1+strokeWidth, textPainter.width/2+infoHeight/2-strokeWidth, -canvasSize.height/2+infoHeight/2+1-strokeWidth,Radius.circular(32.0)))
      ..moveTo(-15+strokeWidth/2, -canvasSize.height/2+infoHeight/2+1-strokeWidth)
      ..lineTo(0, -canvasSize.height/2+infoHeight/2+25-strokeWidth*2)
      ..lineTo(15-strokeWidth/2, -canvasSize.height/2+infoHeight/2+1-strokeWidth)
        , infoPaint);
    textPainter.paint(
        canvas,
        Offset(
            - textPainter.width / 2,
            -canvasSize.height/2-infoHeight/2+infoHeight / 2 - textPainter.height / 2
        )
    );

    canvas.restore();

    // Convert canvas to image
    final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(
        canvasSize.width.toInt(),
        canvasSize.height.toInt()
    );

    // Convert image to bytes
    final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List uint8List = byteData.buffer.asUint8List();

    return BitmapDescriptor.fromBytes(uint8List);
  }*/

}

class Uuid {
  final Random _random = Random();

  String generateV4() {
    // Generate xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx / 8-4-4-4-12.
    final int special = 8 + _random.nextInt(4);

    return '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}-'
        '${_bitsDigits(16, 4)}-'
        '4${_bitsDigits(12, 3)}-'
        '${_printDigits(special, 1)}${_bitsDigits(12, 3)}-'
        '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}';
  }

  String _bitsDigits(int bitCount, int digitCount) => _printDigits(_generateBits(bitCount), digitCount);

  int _generateBits(int bitCount) => _random.nextInt(1 << bitCount);

  String _printDigits(int value, int count) => value.toRadixString(16).padLeft(count, '0');
}
