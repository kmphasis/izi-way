import 'package:flutter/foundation.dart';

class Constant {
  Constant._();

  // static const bool isTestingMode = kDebugMode;

  static const String appName = 'IZI Way';

  // profile type
  static String profileType = "";
  static String profileTypeUser = "User";
  static String profileTypeAgent = "Agent";

  // subscription
  static String isSubscription0 = '0'; // pending subscription
  static String isSubscription1 = '1'; // success subscription
  static String isSubscription2 = '2'; // fail subscription

// free member
  static String isFreeMember0 = '0'; // pending 7 days member ship
  static String isFreeMember1 = '1'; // use 7 days free member ship
  static String isFreeMember2 = '2'; // 7 days free member ship finished.

  // free member ship started
  static String isStarted0 = '0'; // get only data
  static String isStarted1 = '1'; // free member ship started

  // Agreement
  static String isAgreement = ""; // agreement accept pending
  static String isAgreement1 = "1"; // agreement accepted

  // place, google map
  static String apiKey = "AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY";

  // android: AndroidManifest.xml -> <meta-data android:name="com.google.android.geo.API_KEY" android:value="AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY"/>
  // iOS: AppDelegate.swift -> GMSServices.provideAPIKey("AIzaSyAM6jSGzQOSMBpiATlfxYUijJeWzhxqJKY")

  static String markerIdSearch = "101";

  static String validationNullLowerCase = "null";
  static String validationNullUpperCase = "NULL";
  static String deviceTypeAndroid = 'Android';
  static String deviceTypeIos = 'iOS';

  static const String subscriptionPlan1Price = '1000';
  static const String subscriptionPlan2Price = '2500';
  static const String subscriptionPlan3Price = '5000';
  static const String subscriptionPlan4Price = '10000';

  static const String subscriptionPlan1Day = '30';
  static const String subscriptionPlan2Day = '90';
  static const String subscriptionPlan3Day = '180';
  static const String subscriptionPlan4Day = '365';

  static const double zoomLevel = 17;
  static const double zoomPadding = 80;

  // language code
  static const String languageCodeFr = 'fr';
  static const String languageCodeEn = 'en';
  static const String languageCodeDefault = languageCodeFr;

  // language name
  static const String languageNameEnglish = 'English';
  static const String languageNameFrench = 'French (français)';

// date format
  static const String DATE_FORMAT_YYYY_MM_DD_DASH_HH_MM_SS_COLUMN = "yyyy-MM-dd HH:mm:ss"; // 2021-12-28 10:30:12 (YYYY-MM-DD HH:MM:SS (24 hours))
  static const String DATE_FORMAT_DD_MM_YYYY_DASH_HH_MM_COLUMN_AA = "dd-MM-yyyy hh:mm aa"; // 28-12-2021 10:30 AM (DD-MM-YYYY HH:MM AA (12 hours))
}
