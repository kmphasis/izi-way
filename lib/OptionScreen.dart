import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:google_fonts/google_fonts.dart';
import 'HomeScreen.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/screen_size_utils.dart';
import 'package:iziway/EditProfile.dart';
import 'package:iziway/LoginScreen.dart';
import 'package:iziway/MessageScreen.dart';
import 'package:iziway/Model/DataModel.dart';
import 'package:iziway/SubscriptionScreen.dart';
import 'package:iziway/UserListScreen.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'Common/ApiClient.dart';
import 'Common/Utils.dart';
import 'Model/DataObjectResponse.dart';
import 'app_theme.dart';
import 'laguages/Languages.dart';
import 'laguages/locale_constant.dart';

class OptionScreen extends StatefulWidget {
  @override
  _OptionScreenState createState() => _OptionScreenState();
}

class _OptionScreenState extends State<OptionScreen> {
  DataModel userData;
  SharedPref _sharedPref;
  Utils _utils;

  // String name = '', email = '', phoneNo = '';
  // String url = '';
  // bool isNotification = false;

  /* @override
  void didChangeDependencies() async {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }*/

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
    readData();
  }

  /* void _onPressedLogoutClick() {
    Navigator.of(context, rootNavigator: true).pop(); // positive button dismiss
    // click code
    _utils.isNetworkAvailable(context,_utils,showDialog: true).then((value) => checkNetworkLogout(value));
  }*/

  Future<void> readData() async {
    userData = DataModel.fromJson(
        await _sharedPref.readObject(_sharedPref.UserResponse));
    /* setState(() {
      name = userData.name;
      email = userData.email;
      phoneNo = userData.phone;
      url = userData.profile;
      isNotification = userData.is_noti == "0" ? false : true;
    });*/
  }

  // Locale _locale;

  /* void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }*/

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance.init(context);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Column(
          children: [
            Container(
              height: SV.setHeight(650),
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    Constant.profileType == Constant.profileTypeAgent
                        ? 'assets/images/bg_agent_profile.png'
                        : 'assets/images/bg_user_profile.png',
                    fit: BoxFit.fill,
                    width: double.infinity,
                    height: SV.setHeight(500),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        /*left: 20,*/
                        top: (MediaQuery.of(context).padding.top +
                            ResponsiveFlutter.of(context).moderateScale(16))),
                    child: Row(
                      children: [
                        /*SizedBox(
                          height: 40,
                          width: 40,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.black,
                              size: 25,
                            ),
                          ),
                        ),*/
                        Expanded(
                          child: Text(
                            /*"My IZI WAY"*/
                            Constant.appName,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Colors.black),
                          ),
                        ),
                        // SizedBox(width: 50),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Expanded(
                        child: Container(),
                      ),
                      Container(
                        height: SV.setHeight(300),
                        width: SV.setHeight(250),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: SV.setHeight(250),
                              height: SV.setHeight(250),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/app_logo.png'),
                                      fit: BoxFit.cover)),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.only(
                    left: ResponsiveFlutter.of(context).moderateScale(50),
                    right: ResponsiveFlutter.of(context).moderateScale(50),
                    top: ResponsiveFlutter.of(context).moderateScale(16),
                    bottom: ResponsiveFlutter.of(context).moderateScale(16)),
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => HomeScreen()),
                              (Route<dynamic> route) => false);
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ResponsiveFlutter.of(context).scale(12)),
                      width: double.infinity,
                      height: ResponsiveFlutter.of(context).verticalScale(45),
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          color: Constant.profileType==Constant.profileTypeAgent?AppTheme.Yellow:AppTheme.Green,
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).scale(10))),
                      child: Text(
                        Languages.of(context).optionTrafficInfo,
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Constant.profileType==Constant.profileTypeAgent?AppTheme.black:AppTheme.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ResponsiveFlutter.of(context).scale(12)),
                      width: double.infinity,
                      height: ResponsiveFlutter.of(context).verticalScale(45),
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          color: Constant.profileType==Constant.profileTypeAgent?AppTheme.Yellow:AppTheme.Green,
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).scale(10))),
                      child: Text(
                        Languages.of(context).optionBrt,
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Constant.profileType==Constant.profileTypeAgent?AppTheme.black:AppTheme.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ResponsiveFlutter.of(context).scale(12)),
                      width: double.infinity,
                      height: ResponsiveFlutter.of(context).verticalScale(45),
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          color: Constant.profileType==Constant.profileTypeAgent?AppTheme.Yellow:AppTheme.Green,
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).scale(10))),
                      child: Text(
                        Languages.of(context).optionTer,
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Constant.profileType==Constant.profileTypeAgent?AppTheme.black:AppTheme.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ResponsiveFlutter.of(context).scale(12)),
                      width: double.infinity,
                      height: ResponsiveFlutter.of(context).verticalScale(45),
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          color: Constant.profileType==Constant.profileTypeAgent?AppTheme.Yellow:AppTheme.Green,
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).scale(10))),
                      child: Text(
                        Languages.of(context).optionParking,
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Constant.profileType==Constant.profileTypeAgent?AppTheme.black:AppTheme.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: ResponsiveFlutter.of(context).scale(12)),
                      width: double.infinity,
                      height: ResponsiveFlutter.of(context).verticalScale(45),
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          color: Constant.profileType==Constant.profileTypeAgent?AppTheme.Yellow:AppTheme.Green,
                          borderRadius: BorderRadius.circular(
                              ResponsiveFlutter.of(context).scale(10))),
                      child: Text(
                        Languages.of(context).optionEnvironment,
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Constant.profileType==Constant.profileTypeAgent?AppTheme.black:AppTheme.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  /*checkNetwork(bool value) {
    if (value) {
      Future<DataObjectResponse> user = register();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataObjectResponse> register() async {
    try {
      FormData formData = FormData.fromMap({
        'user_id': await _sharedPref.getUserId(),
        'name': name,
        'email': email,
        'phone': phoneNo,
        'is_noti': isNotification == true ? "1" : "0",
        'profile': ''
      });
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.updateProfile, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      if (objectResponse.ResponseCode == 1) {
        //_utils.alertDialog(objectResponse.ResponseMsg);
        storeData(objectResponse);
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }*/

  Future<void> storeData(DataObjectResponse objectResponse) async {
    _sharedPref.saveString(_sharedPref.userType, objectResponse.data.user_type);
    _sharedPref.saveString(_sharedPref.UserId, objectResponse.data.user_id);
    _sharedPref.saveString(_sharedPref.Token, objectResponse.data.token);
    _sharedPref.saveObject(_sharedPref.UserResponse, objectResponse.data);
  }

/*checkNetworkLogout(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = logout();
      user.then((value) => manageResponseLogout(value));
    }
  }

  Future<DataObjectResponse> logout() async {
    try {
      FormData formData =
      FormData.fromMap({'user_id': await _sharedPref.getUserId()});
      Response response = await ApiClient()
          .apiClientInstance(context, await _sharedPref.getToken())
          .post(ApiClient.logout, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponseLogout(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 1) {
        _sharedPref.removeLoginData().then((value) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LoginScreen()),
                  (Route<dynamic> route) => false);
        });
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }*/
}
