import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/HomeScreen.dart';
import 'package:iziway/LoginScreen.dart';

import 'Common/screen_size_utils.dart';
import 'app_theme.dart';
import 'OptionScreen.dart';

class LicenceAgreementScreen extends StatefulWidget {
  @override
  _LicenceAgreementScreenState createState() => _LicenceAgreementScreenState();
}

class _LicenceAgreementScreenState extends State<LicenceAgreementScreen> {
  SharedPref _sharedPref;
  Utils _utils;

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    _sharedPref = SharedPref();
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      // throw Languages.of(context).getMsgUrlLaunch(url);
      _utils.alertDialog(Languages.of(context).getMsgUrlLaunch(url));
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    ScreenUtil.instance.init(context);

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            /*"Licence d'utilisation"*/
            Languages.of(context).titleLicenseToUse,
            style: GoogleFonts.roboto(
                fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
              size: 25,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(20),
                children: [
                  SizedBox(height: 20),
                  Image.asset(
                    'assets/images/ic_licence.png',
                    width: SV.setHeight(350),
                    height: SV.setHeight(340),
                  ),
                  SizedBox(height: 40),
                  /* Text(
                    Languages.of(context).msgLicenseToUse,
                    style: GoogleFonts.roboto(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  )*/

                  // Html(data: Languages.of(context).msgLicenseToUse)

                  RichText(
                    text: TextSpan(
                      style: GoogleFonts.roboto(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: Languages.of(context).msgLicenseToUse_1),
                        TextSpan(text: Languages.of(context).msgLicenseToUse_2),
                        TextSpan(text: Languages.of(context).msgLicenseToUse_3),
                        TextSpan(text: Languages.of(context).msgLicenseToUse_4),
                        TextSpan(
                            text: Languages.of(context).msgLicenseToUse_5,
                            style: TextStyle(
                                color: Colors.blue,
                                decoration: TextDecoration.underline),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                _launchURL(
                                    Languages.of(context).msgLicenseToUse_5);
                              }),
                        TextSpan(text: Languages.of(context).msgLicenseToUse_6),
                        TextSpan(text: Languages.of(context).msgLicenseToUse_7),
                        TextSpan(text: Languages.of(context).msgLicenseToUse_8),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            Row(
              children: [
                SizedBox(width: 20),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()),
                          (Route<dynamic> route) => false);
                    },
                    child: Container(
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: AppTheme.Pink,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'Refuser'*/
                        Languages.of(context).btnRefUser,
                        style: GoogleFonts.roboto(
                            color: AppTheme.DarkRed,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      storeData().then((value) => redirectToOther());
                    },
                    child: Container(
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color:
                              Constant.profileType == Constant.profileTypeUser
                                  ? AppTheme.Green
                                  : AppTheme.Yellow,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'Accepter'*/
                        Languages.of(context).btnAccept,
                        style: GoogleFonts.roboto(
                            color:
                                Constant.profileType == Constant.profileTypeUser
                                    ? AppTheme.white
                                    : Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 30)
          ],
        ),
      ),
    );
  }

  void redirectToOther() {
    _utils.getLocation(_utils).then((value) async {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => OptionScreen()),
          (Route<dynamic> route) => false);
    });
  }

  Future<void> storeData() async {
    _sharedPref.saveString(_sharedPref.isAgreement, Constant.isAgreement1);
  }
}
