import 'dart:convert';
import 'package:iziway/Common/Constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  String UserId = "UserId";
  String UserResponse = "UserResponse";
  String Token = "Token";
  String isAdmin = "isAdmin";
  String isAgreement = "isAgreement";
  String userType = "userType";
  String keyIsRememberMe = "remember_me";
  String keyEmail = "email";
  String keyPassword = "password";

  Future<String> getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(UserId) ?? '';
  }

  Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(Token) ?? '';
  }

  readObject(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key)) ?? null;
  }

  saveObject(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  Future<String> readString(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? '';
  }

  saveString(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  readInt(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.getInt(key);
  }

  readBool(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.getBool(key);
  }

  saveInt(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

  saveBool(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
  }

  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  removeLoginData() async {
    remove(userType);
    remove(UserId);
    remove(UserResponse);
    Constant.profileType = "";
  }

  clearAll() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future<bool> containKey(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }
}
