// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DataObjectResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataObjectResponse _$DataObjectResponseFromJson(Map<String, dynamic> json) {
  return DataObjectResponse()
    ..cancelUrl = json['cancel_url'] as String
    ..returnUrl = json['return_url'] as String
    ..callbackUrl = json['callback_url'] as String
    ..paymentUrl = json['payment_url'] as String
    ..ResponseCode = json['ResponseCode'] as num
    ..ResponseMsg = json['ResponseMsg'] as String
    ..Result = json['Result'] as String
    ..ServerTime = json['ServerTime'] as String
    ..data = json['data'] == null
        ? null
        : DataModel.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DataObjectResponseToJson(DataObjectResponse instance) =>
    <String, dynamic>{
      'cancel_url': instance.cancelUrl,
      'return_url': instance.returnUrl,
      'callback_url': instance.callbackUrl,
      'payment_url': instance.paymentUrl,
      'ResponseCode': instance.ResponseCode,
      'ResponseMsg': instance.ResponseMsg,
      'Result': instance.Result,
      'ServerTime': instance.ServerTime,
      'data': instance.data
    };
