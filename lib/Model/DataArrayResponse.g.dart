// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DataArrayResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataArrayResponse _$DataArrayResponseFromJson(Map<String, dynamic> json) {
  return DataArrayResponse()
    ..ResponseCode = json['ResponseCode'] as num
    ..ResponseMsg = json['ResponseMsg'] as String
    ..Result = json['Result'] as String
    ..ServerTime = json['ServerTime'] as String
    ..data = (json['data'] as List)
        ?.map((e) =>
            e == null ? null : DataModel.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..userDetail = json['user_detail'] == null
        ? null
        : DataModel.fromJson(json['user_detail'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DataArrayResponseToJson(DataArrayResponse instance) =>
    <String, dynamic>{
      'ResponseCode': instance.ResponseCode,
      'ResponseMsg': instance.ResponseMsg,
      'Result': instance.Result,
      'ServerTime': instance.ServerTime,
      'data': instance.data,
      'user_detail': instance.userDetail
    };
