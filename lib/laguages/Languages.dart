import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

abstract class Languages {
  static Languages of(BuildContext context) {
    return Localizations.of<Languages>(context, Languages);
  }

  // String get appName;

  String get selectProfile;

  String get enterName;

  String get enterPhone;

  String get enterEmail;

  String get enterValidEmail;

  String get enterPassword;

  String get enterConPassword;

  String get bothPasswordNotMatch;

  String get enterAgentId;

  String get selectImage;

  String get enterDescription;

  String get enableLocationPermission;

  String get msgError;

  String get msgNotAvailableInternet;

  String get textForte;

  String get textMoyen;

  String get textFaible;

  String get textEnvoyer;

  /*
  String get btnPurchase ; 'Purchase';*/

  String get btnPurchase;

  /*
  String get btnFreeTry ; 'Essai Gratuit pour 7 jours';*/

  String get btnFreeTry;

  String get btnTryNow;

  String get btnSubscription;

  String get btnLogout;

  String get btnOk;

  String get btnNo;

  String get btnYes;

  String get btnRefUser;

  String get btnAccept;

  String get btnParametres;

  String get msgLoading;

  String get msgDeniedForever;

  String get validEnterComment;

  String get msgLocationPermissionDisable;

  String get titleEditProfile;

  String get lblLastName;

  String get validEnterName;

  String get lblEmail;

  String get validEnterEmail;

  String get lblPhoneNo;

  String get validEnterPhoneNo;

  String get btnRecord;

  String get lblForgotPassword;

  String get decForgotPassword;

  String get lblPassword;

  String get validEnterPassword;

  String get btnSubmit;

  String get titleAccessLocalization;

  String get hintSearchLocation;

  String get introduction1;

  String get introduction2;

  String get introduction3;

  String get introduction4;

  String get confirmMsgLogout;

  String get titleLicenseToUse;

  String get msgLicenseToUse;

  String get msgLicenseToUse_1;

  String get msgLicenseToUse_2;

  String get msgLicenseToUse_3;

  String get msgLicenseToUse_4;

  String get msgLicenseToUse_5;

  String get msgLicenseToUse_6;

  String get msgLicenseToUse_7;

  String get msgLicenseToUse_8;

  String getMsgUrlLaunch(String url);

  String get msgLocationPermission1;

  String get hintRememberMe;

  String get btnResendConfirmLink;

  /*
  String get btnToLogin ; 'SE CONNECTER';*/

  String get btnToLogin;

  /*String get btnSubscribe ; 'S\'INSCRIRE';*/

  String get btnSubscribe;

  String get btnMessaging;

  String get hintBy;

  String get hintEnterPassword;

  String get hintEnterConfirmPassword;

  String get lblConfirmPassword;

  String get lblAgentId;

  String get hintEnterId;

  String get lblRegisterAs;

  String get lblUser;

  String get lblAgentEclaireur;

  String get titleMakeAnAlert;

  String get lblByAfricanityGroup;

  String get subscriptionPlan1;

  String get subscriptionPlan2;

  String get subscriptionPlan3;

  String get subscriptionPlan4;

  String get titleSubscriptions;

  String get msgPurchaseFailed;

  String get msgPurchaseSuccess;

  String get lblGoPremium;

  String get lblGetUnlimitedAccess;

  String get contentSubscription;

  String get validSubscriptionPlan;

  String get titleEquipeIZIWay;

  String get titleMyIziWay;

  String get lblUserAccount;

  String get btnSettings;

  String get titlePayment;

  String get btnChangeLanguage;

  String getLocationPermissionDeniedWithName(LocationPermission permission);

  String getDefaultPlan(String planDays, String price);

  String get optionTrafficInfo;

  String get optionBrt;

  String get optionTer;

  String get optionParking;

  String get optionEnvironment;
}
