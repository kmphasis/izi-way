import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_pickers/image_pickers.dart';
import 'laguages/Languages.dart';
import 'package:iziway/Common/Constant.dart';
// import 'package:iziway/Common/SharedPref.dart';
import 'package:iziway/Common/Utils.dart';
import 'package:iziway/LoginScreen.dart';
import 'package:iziway/Model/DataObjectResponse.dart';

import 'Common/ApiClient.dart';
import 'Common/screen_size_utils.dart';
import 'app_theme.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

enum AppState {
  free,
  picked,
  cropped,
}

class _RegisterScreenState extends State<RegisterScreen> {
  Utils _utils;
  // SharedPref _sharedPref;

  var emailFocus = FocusNode(), passwordFocus = FocusNode(), conPassFocus = FocusNode(), mobileFocus = FocusNode();

  File imageCropFile;
  AppState state;

  MultipartFile profileImage;

  String strFileName = '';
  String name = '', phoneNo = '', email = '', password = '', conPassword = '', agentId = '';

  @override
  void initState() {
    super.initState();
    _utils = Utils(context: context);
    // _sharedPref = SharedPref();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    ScreenUtil.instance.init(context);

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Container(
              height: SV.setHeight(650),
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    Constant.profileType == Constant.profileTypeUser ? 'assets/images/ic_top_green.png' : 'assets/images/ic_top_yellow.png',
                    fit: BoxFit.fill,
                    width: double.infinity,
                    height: SV.setHeight(500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20, top: MediaQuery.of(context).padding.top),
                    child: SizedBox(
                      height: 40,
                      width: 40,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                          size: 25,
                        ),
                      ),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Expanded(
                        child: Container(),
                      ),
                      Container(
                        height: SV.setHeight(300),
                        width: SV.setHeight(250),
                        child: GestureDetector(
                          onTap: () {
                            selectImages();
                          },
                          child: Stack(
                            children: <Widget>[
                              Container(
                                width: SV.setHeight(250),
                                height: SV.setHeight(250),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black26,
                                        offset: Offset(0.0, 2.0),
                                        blurRadius: 3.0,
                                      )
                                    ],
                                    image: imageCropFile == null ? DecorationImage(image: AssetImage('assets/images/ic_default_profile.png'), fit: BoxFit.cover) : DecorationImage(image: FileImage(imageCropFile), fit: BoxFit.cover)),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        width: SV.setHeight(90),
                                        height: SV.setHeight(90),
                                        decoration: BoxDecoration(color: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow, border: Border.all(color: AppTheme.OffWhite, width: 3), borderRadius: BorderRadius.circular(360)),
                                        child: Icon(
                                          Icons.add,
                                          color: AppTheme.white,
                                          size: SV.setHeight(60),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(30),
                children: <Widget>[
                  Text(
                    /*'Nom'*/
                    Languages.of(context).lblLastName,
                    style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                      cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                      textInputAction: TextInputAction.next,
                      onChanged: (val) {
                        setState(() {
                          name = val;
                        });
                      },
                      onSubmitted: (v) {
                        FocusScope.of(context).requestFocus(mobileFocus);
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'entrer votre nom'*/ Languages.of(context).validEnterName,
                          hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    /*'Numero de Tel'*/
                    Languages.of(context).lblPhoneNo,
                    style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      onChanged: (val) {
                        setState(() {
                          phoneNo = val;
                        });
                      },
                      style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                      keyboardType: TextInputType.number,
                      cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                      textInputAction: TextInputAction.next,
                      focusNode: mobileFocus,
                      onSubmitted: (v) {
                        FocusScope.of(context).requestFocus(emailFocus);
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'entrer votre Numero de Telephone'*/ Languages.of(context).validEnterPhoneNo,
                          hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    /*'Email'*/
                    Languages.of(context).lblEmail,
                    style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      onChanged: (val) {
                        setState(() {
                          email = val;
                        });
                      },
                      style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                      keyboardType: TextInputType.emailAddress,
                      cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                      textInputAction: TextInputAction.next,
                      focusNode: emailFocus,
                      onSubmitted: (v) {
                        FocusScope.of(context).requestFocus(passwordFocus);
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'Enter your email'*/ Languages.of(context).validEnterEmail,
                          hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    /*'Password'*/
                    Languages.of(context).lblPassword,
                    style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      focusNode: passwordFocus,
                      onChanged: (val) {
                        password = val;
                      },
                      style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                      obscureText: true,
                      cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                      onSubmitted: (v) {
                        FocusScope.of(context).requestFocus(conPassFocus);
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'Enter password'*/ Languages.of(context).hintEnterPassword,
                          hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    /*'Confirm Password'*/
                    Languages.of(context).lblConfirmPassword,
                    style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      focusNode: conPassFocus,
                      onChanged: (val) {
                        setState(() {
                          conPassword = val;
                        });
                      },
                      style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                      obscureText: true,
                      cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                          counter: SizedBox.shrink(),
                          hintText: /*'Enter confirm password'*/ Languages.of(context).hintEnterConfirmPassword,
                          hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(height: 40),
                  GestureDetector(
                    onTap: () {
                      checkValidation();
                    },
                    child: Container(
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(color: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow, borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        /*'Enregistrer'*/
                        Languages.of(context).btnRecord,
                        style: GoogleFonts.roboto(color: Constant.profileType == Constant.profileTypeUser ? AppTheme.white : Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void checkValidation() {
    /* if (imageCropFile == null) {
      _utils.alertDialog(Languages.of(context).selectProfile);
    } else*/
    if (_utils.isValidationEmpty(name)) {
      _utils.alertDialog(Languages.of(context).enterName);
    } else if (_utils.isValidationEmpty(phoneNo)) {
      _utils.alertDialog(Languages.of(context).enterPhone);
    } else if (_utils.isValidationEmpty(email)) {
      _utils.alertDialog(Languages.of(context).enterEmail);
    } else if (!_utils.emailValidator(email)) {
      _utils.alertDialog(Languages.of(context).enterValidEmail);
    } else if (_utils.isValidationEmpty(password)) {
      _utils.alertDialog(Languages.of(context).enterPassword);
    } else if (_utils.isValidationEmpty(conPassword)) {
      _utils.alertDialog(Languages.of(context).enterConPassword);
    } else if (password != conPassword) {
      _utils.alertDialog(Languages.of(context).bothPasswordNotMatch);
    } else {
      if (Constant.profileType == Constant.profileTypeUser) {
        _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => checkNetwork(value));
      } else {
        showAgentDialog();
      }
    }
  }

  checkNetwork(bool value) {
    if (value) {
      _utils.showProgressDialog(Constant.profileType);
      Future<DataObjectResponse> user = register();
      user.then((value) => manageResponse(value));
    }
  }

  Future<DataObjectResponse> register() async {
    try {
      FormData formData = FormData.fromMap({'name': name, 'email': email, 'password': _utils.generateMd5(password), 'phone': phoneNo, 'user_type': Constant.profileType, 'device_type': _utils.getDeviceType(), 'device_token': "token", 'agent_code': agentId, 'profile': imageCropFile != null ? MultipartFile.fromFileSync(imageCropFile.path, filename: imageCropFile.path.split("/").last) : ''});
      Response response = await ApiClient().apiClientInstance(context, '').post(ApiClient.signUp, data: formData);
      return DataObjectResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void manageResponse(DataObjectResponse objectResponse) {
    if (objectResponse.ResponseCode != null) {
      _utils.hideProgressDialog();
      if (objectResponse.ResponseCode == 2) {
        alertDialog(objectResponse.ResponseMsg);
      } else {
        _utils.alertDialog(objectResponse.ResponseMsg);
      }
    }
  }

  void showAgentDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(color: AppTheme.white, borderRadius: BorderRadius.circular(12.0)),
            padding: EdgeInsets.all(30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  /*'Agent ID'*/
                  Languages.of(context).lblAgentId,
                  style: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 40,
                  child: TextField(
                    autofocus: false,
                    onChanged: (val) {
                      setState(() {
                        agentId = val??"";
                      });
                    },
                    style: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400),
                    cursorColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
                    textInputAction: TextInputAction.next,
                    onSubmitted: (v) {
                      FocusScope.of(context).requestFocus(mobileFocus);
                    },
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 10),
                        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppTheme.Grey)),
                        counter: SizedBox.shrink(),
                        hintText: /*'Please enter ID here'*/ Languages.of(context).hintEnterId,
                        hintStyle: GoogleFonts.roboto(fontSize: 15, fontWeight: FontWeight.w400)),
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      if (_utils.isValidationEmpty(agentId)) {
                        _utils.alertDialog(Languages.of(context).enterAgentId);
                      } else {
                        Navigator.pop(context);
                        _utils.isNetworkAvailable(context, _utils, showDialog: true).then((value) => checkNetwork(value));
                      }
                    },
                    child: Container(
                      height: 45,
                      width: SV.setWidth(500),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(color: AppTheme.Yellow, borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        Languages.of(context).btnOk,
                        style: GoogleFonts.roboto(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  void alertDialog(String title) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                // Languages.of(context).appName,
                Constant.appName,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              content: Text(title, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
              actions: <Widget>[
                /* FlatButton(
                  child: Text(Languages.of(context).btnOk),
                  onPressed: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => LoginScreen()),
                        (Route<dynamic> route) => false);
                  },
                ),*/

                _utils.flatButton(Languages.of(context).btnOk, () {
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginScreen()), (Route<dynamic> route) => false);
                }),
              ]);
        });
  }

  Future<void> selectImages() async {
    int wRatio = 1, hRatio = 1;

    try {
      var _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        showGif: false,
        selectCount: 1,
        showCamera: true,
        cropConfig: CropConfig(enableCrop: true, height: hRatio, width: wRatio),
        compressSize: 500,
        uiConfig: UIConfig(
          uiThemeColor: Constant.profileType == Constant.profileTypeUser ? AppTheme.Green : AppTheme.Yellow,
        ),
      );
      _listImagePaths.forEach((media) {
        print(media.path.toString());
        getReturnImage(media.path.toString());
      });
      setState(() {});
    } on PlatformException {}
  }

  getReturnImage(value) {
    if (value != null) {
      setState(() {
        imageCropFile = new File(value);
      });
    }
  }
}
